<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="divimg">
<div class="container">
    <div class="site-login">
        <!--    <h1 id="titulosin">Iniciar Sesión</h1>-->
        <div>




            <?php
            $form = ActiveForm::begin([
             
                        'method' => 'post',
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        ],
            ]);
            ?>
            <div class="row header">

                <h1>CLEVELAND CAVALIERS &nbsp;</h1>
                <ul id="ullogin2">
                    <li><a href='login'>INICIAR SESIÓN</a></li>

                    <li>REGISTRARSE</li>
                </ul>

            </div>
            <div id="campos">
                <!--        <h1 id="identificate">IDENTIFICATE</h1>-->
                <?= $form->field($model, "username")->input("text")->label("Usuario") ?>   
                <?= $form->field($model, "email")->input("email") ?>   
                <?= $form->field($model, "password")->input("password")->label("Contraseña")?>  
                <?= $form->field($model, "password_repeat")->input("password")->label("Repetir contraseña") ?>   




            </div>      <div style="text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;/* margin-right: -30px; */padding-right: 30px;background-color:#EDEDED;/* margin-left: -30px; */">
                <div class="col-lg-offset-1 col-lg-11">


                    <?= Html::submitButton('Registrarme', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>


                <?php ActiveForm::end(); ?>
                <!--<h1 id="allfor">ALL FOR ONE, ONE FOR ALL</h1>-->


            </div>

        </div>
    </div>
</div>
</div>
