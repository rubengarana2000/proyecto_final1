<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="divimg">
<div class="container">
<div class="site-login">
    
        

       
    <?php $form = ActiveForm::begin([
        
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            
        ],
    ]); ?>
    <div class="row header">
      
    <h1>CLEVELAND CAVALIERS &nbsp;</h1>
    <ul id="ullogin1">
    <li>INICIAR SESIÓN</li>
    
    <li><a href='register'>REGISTRARSE</a></li>
   </ul>
  </div>
        <div id="campos">
<!--        <h1 id="identificate">IDENTIFICATE</h1>-->
        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label("Usuario") ?>

            <?= $form->field($model, 'password')->passwordInput()->label("Contraseña")  ?>

      
 </div>      <div style="text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;/* margin-right: -30px; */padding-right: 30px;background-color:#EDEDED;/* margin-left: -30px; */">
           <div class="col-lg-offset-1 col-lg-11">
               
               
                <?= Html::submitButton('Iniciar sesión', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        

    <?php ActiveForm::end(); ?>
<!--<h1 id="allfor">ALL FOR ONE, ONE FOR ALL</h1>-->
    
   
   </div>
   
</div>
</div>
    </div>
  