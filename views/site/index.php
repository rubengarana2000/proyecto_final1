<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
$this->title = 'My Yii Application';
?>

<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <?php
    if (!Yii::$app->user->isGuest) {
        ?>

        <body>

            <div id="bloque1" class="col-lg-4">
                <a href="jugadores/estadisticas">
                    <h1>JUGADORES</h1>
                </a>
                <a href="entrenadores/entrendadoresindex">
                    <h1>ENTRENADORES</h1>
                </a>
                <a href="partidos/resultados">
                    <h1>PARTIDOS</h1>
                </a>
            </div>

            <div  class="col-lg-4">
                <img id="logof" src="../web/img/logo.png" alt="logo"/>
            </div>

            <div id="bloque2" class="col-lg-4">
                <a href="realizan/entrenos">
                    <h1>ENTRENAMIENTOS</h1>
                </a>
                <a href="partidos/maximos">
                    <h1>ESTADÍSTICAS</h1>
                </a>
                <a href="contratos/contratos">
                    <h1>CONTRATOS</h1>
                </a>

            </div>


           

        </body>
        <?php
    } else {
        ?>
        <body>
            <div class="container">
                <div class="row">
                    <div id="noticias">
   <div id="loultimo"style="
    padding-left: 16px;
     padding-right: 10px;
">
                    <h1>LO ÚLTIMO</h1>
                </div>
                        <div  class="col-lg-6">
<a href="site/larry">    <?= Html::tag('div', Html::img('@web/img/imagenlarry.jpg', ['id' => 'noticia1'])); ?></a>
 <p class="ultimanoticia">Reseña de la temporada 2020-21 de Larry Nance Jr.
                        </div>
                        <div id="noticiassub">
                         
                            <div id="notas2"  class="col-lg-6">
                              
                                   <div class="card">
   <a href="site/canton">  <?= Html::tag('div', Html::img('@web/img/charge.jpg', ['id' => 'noticia2'])); ?> </a>
                                    <div class="card-body">
                                        <h5 class="card-title">Canton Charge</h5>
                                        <p class="card-text">El afiliado de los Cavs G-League se muda de Canton a Cleveland.
                                    </div>
                                </div>


                                <div class="card">
   <a href="site/aprender">   <?= Html::tag('div', Html::img('@web/img/imagen1.jpg', ['id' => 'noticia2'])); ?> </a>
                                    <div class="card-body">
                                        <h5 class="card-title">Cavaliers Team</h5>
                                        <p class="card-text">¿Qué hemos aprendido de los cavs esta temporada: 2020-21?</p>

                                    </div>
                                </div>


                                     <div class="card">
    <a href="site/garland">  <?= Html::tag('div', Html::img('@web/img/imagen4 .jpg', ['id' => 'noticia2'])); ?></a>
                                    <div class="card-body">
                                        <h5 class="card-title">Darius Garland</h5>
                                        <p class="card-text">Darius Garland se une al Team USA </p>

                                    </div>
                                </div>


                                    <div class="card">
    <a href="site/okoro">  <?= Html::tag('div', Html::img('@web/img/imagen8.jpg', ['id' => 'noticia2'])); ?></a>
                                    <div class="card-body">
                                        <h5 class="card-title">Isaac Okoro</h5>
                                        <p class="card-text">Isaac Okoro es nombrado en el segundo quinteto de Rookies </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="loultimo">
                    <h1>MÁS NOTICIAS</h1>
                </div>
                <div id="noticiasloultimo">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card2">
   <a href="site/pick3">   <?= Html::tag('div', Html::img('@web/img/usman.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">CAVS ELEGIRÁ EN TERCER LUGAR EN EL NBA DRAFT2021 
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="card2">
   <a href="site/summer">   <?= Html::tag('div', Html::img('@web/img/summer.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">NBA Summer League REGRESA EN AGOSTO</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="card2">
  <a href="site/kevin">    <?= Html::tag('div', Html::img('@web/img/imagen20.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">Kevin Love JUGARá CON EL TEAM USA EN LOS JUEGOS OLÍMPICOS</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="card2">
   <a href="site/coinflip">   <?= Html::tag('div', Html::img('@web/img/draft.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">CAVS PIERDE CONTRA LOS THUNDER EN LA LOTERÍA Y TENDRÁ LA QUINTA PEOR PROBABILIDAD</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card2">
   <a href="site/jarret">   <?= Html::tag('div', Html::img('@web/img/imagen16.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">Los Cavs pueden sacar más provecho de Allen adoptando su potencial</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="card2">
  <a href="site/bestmoments">    <?= Html::tag('div', Html::img('@web/img/imagen12.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">Roundtable: mejores momentos de los Cavs, momentos destacados y más</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="card2">
  <a href="site/mip">    <?= Html::tag('div', Html::img('@web/img/imagen5.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">NBA Reacts: ¿Quién merece ganar el premio al jugador más mejorado?</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="card2">
   <a href="site/publico">   <?= Html::tag('div', Html::img('@web/img/imagen6.jpg', ['id' => 'noticia2'])); ?></a>
                                <div class="card-body">
                                    <h5 class="card-title">SB Nation Reacts: Impacto del público, playoff series y más</h5>
                                    <p class="card-text"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </body>
    <?php
}
?>

</html>