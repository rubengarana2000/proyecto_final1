
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>NBA Summer League REGRESA EN AGOSTO</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img src="../../web/img/summer_1.png" alt=""/>
                </header>

                <p>            
Después de un año de descanso debido al COVID-19, la NBA Summer League regresa este año en agosto. Según la liga, Las Vegas Summer League está programada del 8 al 17 de agosto. Participarán los Cavs y los otros 29 equipos de la NBA.


                </p>
                <br>

                <p>           
Este año, Summer League se debe a que la temporada de la NBA concluya en julio debido a que la temporada 2020-21 comienza tarde debido al COVID-19. La última vez que ocurrió el evento, los Cavs participaron tanto en la liga de Las Vegas como en la liga de Utah. Esto último no parece estar sucediendo este año, al menos por ahora.


                </p>  
                <br>
                <br>
                <p>
Para los Cavs, esto será un posible escaparate de las selecciones del draft de 2021 del equipo, así como del final actual de la lista / jugadores de dos vías como Lamar Stevens y Brodric Thomas. Es posible que Isaac Okoro pudiera jugar, obviamente no tuvo la experiencia el año pasado, pero también podría no hacerlo porque se dirige a su segundo año. (Esta sería una buena pregunta si Cleveland pusiera a Koby Altman a disposición de los medios para hablar sobre los planes de la organización, pero eso no está sucediendo).


                </p>
                <br>
                <br>
                <p>
El baloncesto en la liga de verano no siempre es el mejor, pero es un evento divertido y la señal de que el calendario de baloncesto vuelve a la normalidad. También es de destacar: Summer League comienza apenas 10 días después del Draft de la NBA de 2021. Eso podría resultar en que los jugadores no participen, pero es demasiado pronto para saberlo.


                </p>


               

            </article>

    </div>


</body>



</html>