
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Los Cavs pueden sacar más provecho de Allen adoptando su potencial</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>
                    <img src="../../web/img/imagen16_1.jpg" alt="" style="
    width: 50%;
"/>
                </header>

                <p>       
                    Cuando los Cavaliers, que no son abiertos sobre nada, adquirieron a Jarrett Allen en enero, fueron transparentes acerca de querer a Allen. El gerente general de los Cavs, Koby Altman, en la última vez que respondió preguntas de los medios, lo dijo y lo puso públicamente como parte de los planes futuros del equipo.
                </p>
                <br>

                <p>           
                    Según todas las cuentas, eso no ha cambiado. Allen se perdió algún tiempo con una conmoción cerebral, pero promedió el primer doble-doble de su carrera este año. Tiene 23 años, justo en la línea de Darius Garland, Collin Sexton, Isaac Okoro y el núcleo aún en crecimiento de los Cavs (al menos tal como existe ahora). Con Garland, Allen ya ha desarrollado algo de química con la que Cleveland puede trabajar.

                </p>  
                <br>
                <br>
                <p>
                    Ahora existe la idea de que no vale la pena pagar mucho por los centros. Sin duda, tiene algo de mérito: gran parte del baloncesto moderno se juega en el perímetro y se basa en la versatilidad. Los equipos todavía necesitan grandes, pero algunos están optando por invertir unos pocos millones en un centro en lugar de construir un equipo alrededor de uno.


                </p>
                <br>
                <br>
                <p>
                    En el caso de Allen, es un posible defensor de alto nivel y algo así como un especialista en la ofensiva. Tiene un buen salto desde la línea de tiros libres y ha incursionado en tiros de tres puntos. Allen no se siente cómodo haciendo traspasos de regates o pasando desde los codos o desde la parte superior de la tecla por el momento. (Es lo único que tiene sobre él Isaiah Hartenstein, que sería un buen suplente a un precio justo). Lo que mejor hace es aplastar globos y trabajar alrededor del aro:

                </p>
                <br>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/JfwPbbtTsG0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <p>
                    Tiene cierta capacidad para anotar cuando no se le pasa abierto, lo que ayuda a:

                </p>
                <br>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/P8wmFlNm7ek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <p>
                    Sería bueno verlo probar más la próxima temporada. La creación de jugadas es de lo que los Cavs han hablado más que de tirar, pero esperan un esfuerzo para sumar ambos.

                </p>
                <br>
                <br>
                <p>
                    Defensivamente, sin embargo, Allen ya es realmente bueno. En 51 juegos con los Cavs, Allen tuvo un porcentaje de bloqueo del 2.4%, colocándolo en el 75% superior de los grandes, según Cleaning The Glass. En su carrera, nunca ha tenido una tasa de bloqueo por debajo del promedio, una buena señal de que esto no es una casualidad. Además, solo cometió una falta el 1.8% de su tiempo en la cancha el año pasado, ubicándose en el percentil 99 de los grandes. Esa es la plantilla para un jugador de alto nivel en el que un equipo puede construir una defensa y darle una extensión.

                </p>
                <p>
                    El punto de comparación más obvio aquí para Allen y los Cavs es Capela y los Hawks. (Algunos señalarán al Jazz y Rudy Gobert, pero Gobert es un poco mayor y está otro nivel más alto que los centros de la clase Capela / Allen). Antes de ser canjeado por los Rockets la temporada pasada, firmó un contrato por cinco años y $ 90 millones. eso lo mantiene con los Hawks hasta el 2022-23. La temporada pasada, a los 27 años, Capela tuvo posiblemente su mejor temporada y fue el ancla de la defensa de los Hawks. Atlanta terminó el año 17 en defensa y solo un poco por debajo del promedio de la liga, según Cleaning The Glass, en camino al quinto sembrado en el Este.

                </p>
                <br>
                <br>
                <p>
                    Lo que hacen los Hawks y Jazz es ofrecer un modelo de lo que los Cavs pueden hacer el próximo año. El esquema general será en gran parte de vainilla: pocos cambios, sin intentar que los jugadores más pequeños salgan de las publicaciones, solo la rara trampa dura o blitz, pero se basará en principios sólidos. Allen será el centro de eso. En el pick-and-roll, caerá, retrocederá e intentará defender a dos jugadores, y permitirá que todos los demás se queden en casa en los tiradores. No es complicado, pero puede funcionar.

                    <br>
                    <br>
                <p>
                    Mira cómo Capela defiende aquí. Siempre está en alguna versión de ayuda, sin perder de vista la pintura. Si está cubriendo a un guardia que es golpeado, aquí está para frenar al manejador de la pelota oponente y aún estar en posición para bloquear el tiro. O si se está recuperando para una defensa rota, todavía está al acecho y no se compromete demasiado ni se pone fuera de posición.


                </p>
                <br>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/b2g0KnvyyKI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <p>
                    Y mira cómo funciona Gobert aquí. Ninguno de los otros defensores de Utah es perfecto e incluso Mike Conley Jr. en el punto de ataque es pequeño y atacante. Pero son tan disciplinados que no importa. Todo está construido y empujándolo en Gobert.


                </p>
                <br>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/xhcfesZEMDQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <p>
                    Los Cavs ya están haciendo algo de esto. Casi exclusivamente jugaron la cobertura de caída el año pasado con un poco de zona mezclada fuera de los tiempos muertos y en otros lugares. El problema es que Allen llegó a mitad de temporada y no había tiempo real para instalar y afinar en medio de una temporada condensada. Si eres Cleveland, la esperanza es que un campo de entrenamiento y una pretemporada real con tiempo de práctica sean suficientes para aprender los principios y construir a partir de ahí. También podría necesitar agregar algo de fuerza para manejar el ataque una y otra vez por creadores más grandes. Allen pesa 235 libras, pero parece flaco.


                </p>
                <p>
                    También hay algunas preguntas sobre la lista, y Kevin Love se destaca como la más urgente. Cuando está comprometido y dispuesto, puede ayudar en ese extremo rebotando, esforzándose y estando en los lugares correctos. Me encanta agarrar los tiros bloqueados de Allen y disparar en la cancha para comenzar un contraataque suena genial en el papel. Pero, ¿qué pasa si holgazanea y obliga a Allen a cubrirlo más que a ser el centro del esquema defensivo? Si ese equilibrio se sale de control, toda la columna vertebral de la defensa de los Cavs se derrumba. Allen, o cualquier otro grande, solo puede cubrir un espacio limitado.


                </p>
                <br>
                <br>
                <p>
                    Hay problemas de espacio y salud, pero podría ser más fácil construir de esta manera con Larry Nance Jr. en el cuarto con Taurean Prince y tal vez una selección del draft como Jalen Johnson o Franz Wagner obteniendo algunos minutos en el lugar. Quizás Lamar Stevens tenga la oportunidad de ganar minutos el año que viene. Quizás Dean Wade pueda hacer lo suficiente para que funcione y ofrezca un mejor espaciado. Un agente libre de compra baja como Rondae Hollis-Jefferson u Otto Porter tal vez podría tener sentido.


                </p>
                   <p>
                El cuarto año para Collin Sexton y el año tres para Darius Garland también deben producir algunas mejoras. Por más divertidos que puedan ser en la ofensiva, descifrarlos en la defensa es esencial para construir con ambos. Y, para ser franco, es muy difícil construir una defensa con guardias más pequeños. Los Jazz lo hacen, pero Gobert es de otro mundo. Conley es también uno de los jugadores más inteligentes de la liga como defensor de punto de ataque y eso. Eso tiene que ofrecer algo de impulso a Gobert.


                </p>
                <p>
                    La esperanza aquí tiene que ser que ambos hagan mejoras a medida que avanzan sus carreras y que Sexton, con su energía sin límites y su envergadura decente de 6'6 ”, pueda convertirse en una plaga en el punto de ataque en las líneas de adelantamiento. (El año pasado, Sexton tuvo una tasa de robos por debajo del promedio. Para ser justos, también lo tiene Mitchell. Definitivamente hay un cálculo de riesgo aquí en quedarse en casa versus apostar para obtener robos. El Jazz, por elección, no hace todo lo posible búsqueda de pérdidas de balón.) En comparación, los Hawks han hecho todo lo posible alrededor de Young y juegan con un defensor posiblemente peor que Love en Danilo Gallinari junto a Capela.



                </p>
                <br>
                <br>
                <p>

                    Esto también llevará tiempo resolverlo. Para el Jazz, tomó algunos años no solo para que Gobert creciera, sino también para que el equipo se completara y para que Quin Snyder llevara su esquema a donde está ahora. Para los Hawks, dieron un salto al 17 desde el 28 en 2019-20. También estuvieron en el puesto 11 en defensa desde el 1 de marzo , cuando Nate McMillan reemplazó a Lloyd Pierce, hasta el final de la temporada regular. Definitivamente hay algo en el entrenador que ayuda en ese aspecto. Para que esto realmente funcione, los Cavs deben darle tiempo a JB Bickerstaff para trabajar con el roster y construir algo. Probablemente también necesiten mantener a Koby Altman, ya que construyó la lista como está ahora. No deberían tener una pista ilimitada, pero también necesitan tiempo para intentar llegar a alguna parte.


                </p>
                <br>
                <br>
                <p>

                    Y por lo que vale: los Cavs tenían un índice defensivo de 117.1 con Allen en la cancha el año pasado y 113.2 sin él. Pero los datos de la alineación varían mucho de una alineación a otra, por lo que es difícil dar demasiada importancia a estos números. La temporada 2020-21, francamente, fue muy extraña.


                </p>
                <p>
                    Este equipo podría estar cerca de estar al menos en la búsqueda del juego. Ganar en la NBA requiere tiempo, paciencia y algo de suerte. Si los Cavs hacen cambios drásticos porque están impacientes, un sello distintivo de la era de Dan Gilbert, entonces solo dolerá. Cosechar, sembrar, etc.



                </p>
                <br>
                <br>
                <p>
                    Allen también necesita tiempo para crecer en esto. Tiene 23 años y, cuando alguien en su rango de edad firma un trato, al menos se basa parcialmente en las ventajas. Para ver si puede llegar allí, los Cavs harían bien en construir alrededor de Allen en el lado defensivo de la cancha y darle tiempo para ver a dónde va.



                </p>




               

            </article>

    </div>


</body>



</html>