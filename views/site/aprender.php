
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>¿Qué hemos aprendido de los cavs esta temporada: 2020-21?</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img id="noticia" src="../../web/img/collinoticia.jpg" alt=""/>
                </header>

                <p>
                    La temporada finalmente ha llegado a su fin después de un brutal tramo de 1-14 para cerrarla. Las lesiones eclipsaron esta temporada ya que los Cavs nunca pudieron establecer un impulso sostenido. A pesar de las alineaciones inconsistentes, el núcleo joven de Collin Sexton, Darius Garland, Isaac Okoro y Jarrett Allen mostró signos prometedores individual y colectivamente.

                    Esto es algo de lo que aprendimos esta temporada.
                </p>

                <h2>Las lesiones mataron a este equipo.</h2>
                <p>           
                    La alineación de cinco hombres más utilizada de los Cavs (Garland / Sexton / Okoro / Nance / Allen) jugaron solo 141 minutos y 10 juegos en total juntos. La alineación inicial proyectada actual para la próxima temporada de Garland, Sexton, Okoro, Love y Allen jugó solo 7 juegos y 85 minutos juntos. De alguna manera, esa seguía siendo la tercera alineación más utilizada del equipo. Esta falta de alineaciones consistentes hace que sea increíblemente difícil evaluar lo que tienes y lo que necesitas alrededor de tus jugadores jóvenes en una temporada baja fundamental.                
                </p>  

                <br>
                <p>
                    Las lesiones aniquilaron a los veteranos de este equipo, ya que Larry Nance Jr., Kevin Love, Taurean Prince y Matthew Dellavedova se limitaron a 35 juegos o menos esta temporada. El hecho de que los cuatro veteranos se perdieran más de la mitad de la temporada obligó a los Cavs a jugar en la liga G y los jugadores del final de la lista durante minutos significativos, al tiempo que obligó al núcleo joven a llevar toda la carga en ambos extremos. Esta es una de las principales razones por las que los Cavs ocuparon el puesto 28 en la clasificación neta general.                
                </p>

                <br>
                <p>
                    Ganar partidos de baloncesto es difícil. Es especialmente así cuando no puedes contar con los mismos chicos para estar ahí todas las noches. La mejor habilidad es la disponibilidad. No había muchos Cavaliers disponibles noche tras noche y los resultados lo demuestran.                
                </p>
                <h2>Los Cavs necesitan más de sus veteranos</h2>

                <p>
                    Los equipos jóvenes tendrán una variación extremadamente alta. Una noche, Sexton y Garland se verán geniales. El siguiente, no tanto. Eso es normal y forma parte del proceso de crecimiento de los jugadores jóvenes. Por eso es casi imposible depender únicamente de los jóvenes para llevar un equipo. Los Cavs se vieron obligados a vivir y morir por el juego de su pista trasera la mayoría de las noches debido a la falta de apoyo de los veteranos.                
                </p>

                <br>
                <p>
                    Los veteranos fueron los más afectados por las lesiones, pero incluso cuando jugaban, no siempre eran buenos. Love armó su peor temporada como profesional cuando terminó la temporada con un mínimo de puntos en su carrera (12.2) y un mínimo en rebotes (7.4) en los juegos limitados que jugó. Love también luchó por encontrar su tiro, ya que terminó con su segundo peor porcentaje de tiros reales (55.6%) desde que llegó de Minnesota.                
                </p>

                <br>
                <p>
                    La temporada de Larry Nance Jr. fue algo mixta. Su defensa fue excepcional para comenzar la temporada, ya que encajó perfectamente junto a Andre Drummond en el lado defensivo (105.5 de calificación defensiva en 445 minutos juntos) al comienzo de la temporada. Sin embargo, Nance no pudo replicar ese mismo techo defensivo con Allen. El equipo tuvo problemas en ese extremo cuando Allen y Nance compartieron la cancha (índice defensivo 111.0 en 347 minutos) y parecían tener habilidades superpuestas en el lado ofensivo (índice ofensivo 101.3).                
                </p>
                <p>
                    El buen juego de Nance en el lado defensivo se vio ensombrecido por su juego ofensivo estancado y su ajuste con algunos de los jugadores jóvenes. Nance simultáneamente hace demasiado y no lo suficiente en el lado ofensivo. Es un manejador de pelota capaz y un creador de juego secundario, ya que promedió 3.1 asistencias sólidas por juego. Sin embargo, cae en la trampa de tratar de hacer demasiado en ese departamento, ya que promedió un récord personal en pérdidas de balón (1.6) que casi contrarresta la increíble cantidad de robos que consiguió en el otro lado (1.7). Además, es cada vez más claro que Nance no se siente cómodo disparando las tres bolas lo suficiente como para proporcionar espacio. Nance disparó un respetable 36.0% desde lo profundo, pero aún está intentando solo 3.3 intentos por juego. Su caída en el porcentaje de tiros desde la línea esta temporada (61.                
                </p>

                <br>
                <p>
                    Nance, como casi todos los demás en el equipo, se ha adaptado perfectamente a Garland. Sin embargo, no se puede decir lo mismo sobre su adaptación junto a Sexton y Allen. Sexton y Nance no han encontrado la forma de coexistir fácilmente. A veces, parece que sus personalidades y estilos de juego aún chocan, ya que Nance parece no confiar plenamente en Sexton con el balón. El ajuste con Allen no es perfecto ya que ambos jugadores se usan mejor en lugares similares en el lado ofensivo. Una temporada baja completa juntos podría ayudar a resolver las diferencias en el juego de Allen y Nance.                
                </p>
                <p>
                    Los Cavs también necesitaban más de Cedi Osman esta temporada. El banco necesitaba disparos desesperadamente y alguien que pudiera proporcionar energía y juego secundario que Osman no pudo entregar hasta las últimas semanas. Osman tuvo su peor temporada de tiros, ya que terminó con divisiones de tiros de .374 / .306 / .800 y un porcentaje efectivo de tiros de campo del 45.8%. Esto está en marcado contraste con el 38,3% de disparos desde la distancia en 4,9 intentos por partido que promedió la temporada pasada.                
                </p>
                <br>

                <p>
                    La utilidad de Osman depende casi por completo de sus disparos. Es sólido en muchas áreas, pero no es bueno en nada cuando su tiro exterior no cae. Volver al jugador que era en 2019-20 sería de gran ayuda para estabilizar una unidad de banco que fue un punto problemático durante toda la temporada.                
                </p>
                <br>

                <p>
                    Equipos jóvenes como los Hawks y los Hornets pudieron llegar a los playoffs y jugar porque pudieron rodear a sus jugadores jóvenes con agentes libres veteranos como Bogdan Bogdanovic, Danilo Gallinari y Terry Rozier. Es poco probable que los Cavs tengan un camino similar para dar ese salto la próxima temporada. Los Cavs no están en posición de agregar veteranos significativos en la agencia libre fuera de la excepción de nivel medio debido a su situación actual en el tope salarial .                
                </p>
                <p>
                    Si Cleveland va a dar un salto similar la próxima temporada, será porque Love, Nance y Osman pueden mantenerse saludables y jugar a la altura de sus estándares. Si no pueden, la posición de los Cavs en la clasificación probablemente no mejorará significativamente en 2021-22                
                </p>
                <br>

                <p>
                    Nance, como casi todos los demás en el equipo, se ha adaptado perfectamente a Garland. Sin embargo, no se puede decir lo mismo sobre su adaptación junto a Sexton y Allen. Sexton y Nance no han encontrado la forma de coexistir fácilmente. A veces, parece que sus personalidades y estilos de juego aún chocan, ya que Nance parece no confiar plenamente en Sexton con el balón. El ajuste con Allen no es perfecto ya que ambos jugadores se usan mejor en lugares similares en el lado ofensivo. Una temporada baja completa juntos podría ayudar a resolver las diferencias en el juego de Allen y Nance.                
                </p>
                

               

            </article>

    </div>


</body>



</html>