
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Roundtable: mejores momentos de los Cavs, momentos destacados y más</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img id="noticia" src="../../web/img/imagen12_1.jpg" alt=""/>
                </header>
                <h2>Tu momento favorito de la temporada de los Cavs es ...</h2>

                <p>
                    Will Cunningham (@willocunningham): Para mí, es sin duda la victoria sobre Brooklyn cuando Collin Sexton anotó 42 puntos. No solo fue un juego increíble y la mejor actuación de un jugador de los Cavs desde probablemente LeBron en el Juego 1 de las Finales de 2018, sino que representó la marca de agua más alta de la esperanza para el futuro de la franquicia. Koby Altman acababa de aterrizar a Jarrett Allen al lanzarse al intercambio de James Harden, y Sexton quemó a los Nets en el primer juego de Harden, golpeando un timbre para enviar el juego al doble de la prórroga y anotando 20 puntos consecutivos para rematar una victoria de los Cavs. .
                </p>


                <p>           
                    Nick Trizzino (@trizweino): Sexton vaporizando los Nets no es la opción más original. Pero, quiero decir ... yeeeeeesh. Una cosa es cuando un jugador aparece en los titulares por un arrebato de anotaciones para lograr una victoria. Otra cosa es cuando tu estrella en ciernes traslada a tu equipo al centro del universo del baloncesto convirtiéndose en supernova en el Voltron recién ensamblado de la liga. Puede que no se sostenga a largo plazo, pero al menos durante una noche, los Cavs volvieron a tener una megaestrella propia, y se sintió muy bien.

                </p>  

                <br>
                <p>
                    Leah Nemeth: (@leah_nemeth): La victoria de principios de abril por 125-101 sobre los San Antonio Spurs cuando Darius Garland aseguró el control completo del juego en la punta y sumó 37 puntos, la mayor cantidad de su carrera. Fue entonces que Garland me conquistó no solo con sus habilidades, sino también con su confianza como jugador joven en la liga. Esa misma noche, el grande recién adquirido de los Cavs, Isaiah Hartenstein, anotó un doble-doble liderando al equipo en rebotes, derribando 12 rebotes y aportando 16 puntos. Estos dos jugadores han mostrado una gran promesa para Cleveland, y estuvo en plena exhibición esa noche contra los Spurs.

                </p>

                <br>
                <p>
                    Chris Manning (@cwmwrites): El final de Cavs-Hawks donde Lamar Stevens hizo una volcada ganadora. Fue un baloncesto apretado y lleno de tensión y eso no fue algo común en todo este año. Y el final fue tan aleatorio y extraño. Me encantan los juegos como este.

                </p>
                <h2>Tu jugador favorito para ver durante la temporada de los Cavs fue ...
                </h2>


                <p>
                    WC: Darius Garland. Mejoró mucho este año y fue muy divertido ver su desarrollo, especialmente en lo que respecta a su fallecimiento. Garland y Allen tuvieron una química inmediata en el pick-and-roll, y eso le permitió a Garland mostrar sus habilidades de pase mejoradas, encontrando consistentemente a Allen con pases de rebote hábiles, volcados rápidos y globos de alto arco. Garland demostró que puede ser una amenaza legítima como creador en esta liga, y su desarrollo fue una de las partes más importantes de esta temporada.

                </p>

                <br>
                <p>
                    LN: Isaac Okoro. Elegir el quinto en el tercer año de una reconstrucción no es exactamente vivir en Easy St., es una apuesta legítima. Okoko se vistió casi todas las noches y se entregó a sí mismo. Comenzó la temporada ganando sus fichas a la defensiva y, mano tras mano , llamó al farol de todos cerrando al mejor jugador de la cancha. El novato jugó 67 juegos, el máximo del equipo, y mostró una mejora constante en todos los aspectos del baloncesto. Fue emocionante verlo hasta el final, el amargo final .

                </p>

                <br>
                <p>
                    NT: A pesar de sus defectos, diablos, es tóxico, ¿pero posiblemente debido a ellos? - es Collin Sexton. Es un anotador avanzado con un saltador de rango medio casi automático y un flotador tan plumoso que podrías usarlo como almohada. También es un anotador insaciable que se enfrentaría a toda la Liga de la Justicia si un cubo estuviera esperando al otro lado. A veces, la confianza imposible de Sexton genera tensiones con los compañeros de equipo y hechizos de ineficacia. Otras veces, se desborda y Sexton detona 15 puntos en un cuarto. Independientemente de lo que piense de su juego, Sexton rara vez es una experiencia de visualización aburrida.

                </p>
                <p>
                    CM: Cualquiera de los tres jugadores ya mencionados son buenas respuestas. Probablemente daría una respuesta diferente en un día diferente. Pero soy un tonto por lo que Okoro es y hace. Muestra mucho potencial y juega muy duro todo el tiempo. Ignore las métricas defensivas: los analistas de la liga le dirán que es difícil valorarlos frente a la cinta del juego. Okoro ya es realmente bueno y mostró suficiente talento ofensivo que ofrece más ventajas que sus números en bruto. No puedo esperar a ver qué tipo de jugador será en el segundo año.

                </p>

                <br>
                <h2>Tu actuación favorita de cualquier Cavalier este año es ……
                </h2>

                <p>
                    WC: Los 42 puntos antes mencionados de Sexton contra los Nets. Agregó 5 puntos y 5 asistencias y disparó al 55% desde el piso y 5/11 desde tres. Fue eficiente y dio un paso al frente cuando el equipo más lo necesitaba, anotando 25 de sus 42 puntos en el último cuarto y las dos prórrogas. Además, Garland se perdió este juego, por lo que Sexton tuvo que llevar la ofensiva e hizo un trabajo admirable. Este fue el tipo de juego en el que vimos lo mejor de Sexton, y es la razón por la que algunos fanáticos son tan optimistas sobre él.

                </p>
                <p>
                    LN: el récord personal de Garland de 37 con los Spurs; 14 de 22 disparos (5-10 de tres). Por favor vea arriba.

                </p>
                <br>

                <p>
                    NT: Si hay algo que amo más que la variedad por sí misma, es el juego central dominante. Es por eso que estoy rodando con la primera apertura de Jarrett Allen contra los Minnesota Timberwolves . El Fro inició la velada con un intento de volcada en su primera posesión defensiva. Dejó caer un yunque de un globo de Garland para un bis. Por llamada de telón, Allen había empujado a Minny para 23 puntos, 18 rebotes y 5 tapones. No hay nada como ver a un gran jugador controlar la pintura durante 48 minutos. ¿Cuándo fue la última vez que los Cavs hicieron que alguien hiciera eso?

                </p>
                <br>

                <p>
                    CM: Los juegos de Sexton y Garland mencionados son las elecciones correctas; verlos cada uno de ellos este año da esperanza para el futuro. Pero para ser diferente, iré con el juego de 17 puntos de Okoro en una derrota contra el Heat en abril. Los números de puntaje de caja no son enormes y no tiene un hito adjunto como él anotando 30 puntos. (Lo cual, para que conste, lo hizo más adelante en la temporada). Pero su defensa en este juego estuvo en el punto dentro y fuera de la pelota y mostró qué tipo de jugador de dos vías puede ser. Este es un juego que, si quieres echar un vistazo a lo que puede ser, es uno para volver a visitar.

                </p>
                <h2>Dame tu punto culminante favorito de los Cavs.

                </h2>
                <p>
                    LN :

                </p>
                <br>
                <div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 480px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH0sInRmd190ZWFtX2hvbGRiYWNrXzExOTI5Ijp7ImJ1Y2tldCI6InByb2R1Y3Rpb24iLCJ2ZXJzaW9uIjo3fX0%3D&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1384305173988741127&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F5%2F20%2F22444893%2Fcleveland-cavaliers-roundtable-best-moment-highlights-collin-sexton&amp;sessionId=d211849fdd9ba485bacc77229b19732db4f8631d&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1384305173988741127"></iframe></div>

                <p>
                    WC:

                </p>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/P8wmFlNm7ek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>
                    NT: Darius Garland lanzó algunos pases alucinantes este año. Esta ayuda (¡y este ángulo de cámara!) Me tenía a punto de desmayarme.

                </p>
                <br>

                <p>
CM: Este fue el primero de muchos lobs por venir:


                </p>
<iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 480px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-1&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH0sInRmd190ZWFtX2hvbGRiYWNrXzExOTI5Ijp7ImJ1Y2tldCI6InByb2R1Y3Rpb24iLCJ2ZXJzaW9uIjo3fX0%3D&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1352802025440874496&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F5%2F20%2F22444893%2Fcleveland-cavaliers-roundtable-best-moment-highlights-collin-sexton&amp;sessionId=d211849fdd9ba485bacc77229b19732db4f8631d&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1352802025440874496"><div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 480px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH0sInRmd190ZWFtX2hvbGRiYWNrXzExOTI5Ijp7ImJ1Y2tldCI6InByb2R1Y3Rpb24iLCJ2ZXJzaW9uIjo3fX0%3D&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1384305173988741127&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F5%2F20%2F22444893%2Fcleveland-cavaliers-roundtable-best-moment-highlights-collin-sexton&amp;sessionId=d211849fdd9ba485bacc77229b19732db4f8631d&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1384305173988741127"></iframe></div></iframe>




                

            </article>

    </div>


</body>



</html>