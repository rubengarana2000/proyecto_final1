
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>CAVS PIERDE LA LOTERÍA Y TENDRÁ LA QUINTA PEOR PROBABILIDAD</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>
                    
                    <img src="../../web/img/draft_2.jpg" alt="" style="
    width: 60%;
"/>
                </header>

                <p>          
Los Cavs recibieron noticias desafortunadas sobre el draft el martes.

                </p>
                <br>

                <p>           
Según la NBA, los Cavs perdieron su lanzamiento de moneda con el Thunder para ver quién obtendría las probabilidades de la lotería del cuarto mejor draft y la quinta mejor. Debido a que los Cavs perdieron, tienen lo último. Aquí están sus probabilidades de draft completas:

                </p>  
                         <br>
                                  
                                  <div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 659px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH19&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1397252985319743488&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F5%2F25%2F22454090%2Fcleveland-cavaliers-2021-draft-lottery-coin-toss&amp;sessionId=26a2adcb8c68189edef4b5e6ea739da78d4ef870&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1397252985319743488"></iframe></div>
                  <br>
                                  <p>
También es de destacar: Los Cavs ganaron lanzamientos de monedas de lotería y luego terminaron con una selección peor antes. Mire hacia atrás en 2012, cuando Cleveland ganó un lanzamiento de moneda con Nueva Orleans. Los Pelicans luego terminaron ganando la lotería para obtener a Anthony Davis y los Cavs eligieron cuarto y obtuvieron a Dion Waiters. Esto es realmente solo suerte al final del día.


                </p>
                         <br>
                                  <br>
                <p>
La Lotería del Draft de la NBA 2021 , también conocida como cuando es el momento de preocuparse realmente por las pelotas de ping pong, está programada para el 22 de junio.

                </p>
          

            </article>

    </div>


</body>



</html>