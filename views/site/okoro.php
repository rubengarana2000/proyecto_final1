
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Isaac Okoro es nombrado en el segundo quinteto de Rookies

</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img id="noticia" src="../../web/img/garlandnoticia.jpg" alt=""/>
                </header>

                <p>
                    El alero de los Cleveland Cavaliers , Isaac Okoro, ha sido nombrado para el Segundo Equipo All-Rookie de la NBA según un comunicado de la liga.


                </p>
                <br>

                <p>       
                    Okoro, la quinta selección de los Cavs de Auburn en noviembre pasado, recibió 53 puntos en total y recibió un voto del primer equipo para formar parte del segundo equipo junto a Patrick Williams de Chicago, Immanuel Quickley de Nueva York, Isaiah Stewart de Detroit y Desmond Bane de Memphis.


                </p>  
                <br>
                <br>
                <div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 858px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH19&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1405680438581018632&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F6%2F17%2F22539483%2Fisaac-okoro-cleveland-cavaliers-all-rookie-second-team&amp;sessionId=f7a4b35c42e1427c3a8b452119624088f67aad1c&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1405680438581018632"></iframe></div>
                 <br>
                <p>
El primer equipo All-Rookie está formado por el base de los Hornets y el novato del año LaMelo Ball, el base de los Timberwolves Anthony Edwards, el base de los Kings Tyrese Haliburton, el alero de los Rockets Jae'Sean Tate y el ala de los Pistons Saddiq Bey.

                </p>
                <br>
                <br>
                <p>
El caso de Okoro para las tragamonedas All-Rookie no es infalible, pero es fuerte y se merece esta ranura. Fue lanzado al fondo en defensa, tuvo éxito y agregó poco a poco a la ofensiva a medida que avanzaba el año. A medida que se dirige al segundo año y obtiene una temporada baja sin impacto de COVID-19 para trabajar en su juego, será fascinante ver cuánto puede crecer Okoro.

                </p>
              

              

            </article>

    </div>


</body>



</html>