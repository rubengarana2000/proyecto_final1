<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use kartik\export\ExportMenu;
$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'ENTRENAMIENTOS';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
<?php
        if (Yii::$app->user->identity->admin) {
            ?>
        <h1 id="titulo"><?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'label' => 'Nombre',
                    'value' => function ($model) {
                        return $model->nombre . ' ' . $model->apellidos;
                    }
                ],
                'distancia_jugador',
                'calorias_jugador',
                [
                    'attribute' => 'fecha',
                    'label' => 'fecha',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);?><?= Html::encode($this->title) ?>  <?= Html::a('Añadir Entrenamiento', ['create'], ['class' => 'btn']) ?></h1>
        


       
        <input type="text" id="buscador" class="form-control" placeholder="Buscar...">
        <?=
        GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'label' => 'Nombre',
                    'value' => function ($model) {
                        return $model->nombre . ' ' . $model->apellidos;
                    }
                ],
                'distancia_jugador',
                'calorias_jugador',
                [
                    'attribute' => 'fecha',
                    'label' => 'fecha',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha));
                    }
                ],
                 ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
        ],
            ],
        ]);
        ?>
          <?php
        } else {
            ?>
 <h1 id="titulonoadmin"><?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'label' => 'Nombre',
                    'value' => function ($model) {
                        return $model->nombre . ' ' . $model->apellidos;
                    }
                ],
                'distancia_jugador',
                'calorias_jugador',
                [
                    'attribute' => 'fecha',
                    'label' => 'fecha',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);?><?= Html::encode($this->title) ?></h1>
        


       
        <input type="text" id="buscador" class="form-control" placeholder="Buscar...">
        <?=
        GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'label' => 'Nombre',
                    'value' => function ($model) {
                        return $model->nombre . ' ' . $model->apellidos;
                    }
                ],
                'distancia_jugador',
                'calorias_jugador',
                [
                    'attribute' => 'fecha',
                    'label' => 'fecha',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha));
                    }
                ],
           
            ],
        ]);
                 }
        ?>



    </div>

</div>
<script>
    $("#buscador").on("keyup", function () {

        var value = $(this).val().toLowerCase();
//  if(!$.isNumeric(value)){
        $(".table tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
//    }
    });

</script>


