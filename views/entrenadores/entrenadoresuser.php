<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = ['label' => 'entrenadores', 'url' => ['consulta2']];
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
      
            <h1 id="titulonoadmin">
               
                <?= Html::encode($this->title) ?></h1>



            <?=
            GridView::widget([
                'dataProvider' => $resultados,
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'value' => function ($data) {
                            return Html::a($data->nombre . " " . $data->apellidos);
                        },
                        'format' => 'raw',
                    ],
                    'cargo',
                    
                ],
            ]);
        
        ?>


    </div>
</div>

