<?php
use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <div class="container">
<h1 id="titulosin">ENTRENADORES</h1>
<?php
 $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'nombre'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Nombre del entrenador']],
        'apellidos'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Apellidos del entrenador']],
        

    ]
]);
echo Form::widget([
    'model'=>$model,
   
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
       'cargo'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['Principal' => 'Principal', 'asistente' => 'Asistente', 'desarrollo' => 'Desarrollo','tiro'=>'Tiro','defensa'=>'Defensa','ataque'=>'Ataque','preparador'=>'Preparador'], 'options'=>['placeholder'=>'Enter password...']],
        'telefono'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Número de telefono']]

    ]
]);
echo "<br>";
echo "<br>";
echo Form::widget([
    'model'=>$model,
   
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        ''=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color:#EDEDED;margin-left: -30px;;">' . 
              Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                '</div>'
        ],

    ]
]);





ActiveForm::end();
?>
</div>
</div>