<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

//
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>


        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/img/c.ico" type="image/x-icon" />
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="../web/js/jquery.js"></script>

        <?php $this->registerCsrfMetaTags() ?>
        <title>Cleveland Cavaliers</title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">


            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/img/c.png', ['id' => 'logo']),
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            if (!Yii::$app->user->isGuest) {




                echo Nav::widget([
                    'encodeLabels' => false,
                    'options' => ['id' => 'list', 'class' => 'navbar-nav navbar-left'],
                    'items' => [
                        ['label' => 'Gestión Deportiva',
                            'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                            'items' => [
                                ['label' => 'Jugadores', 'url' => ['jugadores/estadisticas']],
                                ['label' => 'Entrenadores', 'url' => ['entrenadores/entrendadoresindex']],
                                ['label' => 'Calendario', 'url' => ['partidos/resultadospormes']],
                                ['label' => 'Entrenamientos', 'url' => ['realizan/entrenos']],
                                ['label' => 'Agentes libres', 'url' => ['jugadores/agentes']],
                                ['label' => 'Renovaciones', 'url' => ['contratos/renovaciones']],
                            ],
                        ],
                        ['label' => 'Finanzas',
                            'url' => ['contratos/contratos'],
                            'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                            'items' => [
                                ['label' => 'Contratos', 'url' => ['contratos/contratos']],
                                ['label' => 'Margen Salarial', 'url' => ['contratos/margen']],
                            ],
                        ],
                        ['label' => 'Estadísticas',
                            'url' => ['#'],
                            'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                            'items' => [
                                ['label' => 'Jugadores', 'url' => ['jugadores/estadisticas']],
                                ['label' => 'Partidos', 'url' => ['partidos/resultados']],
                            ],
                        ],
                        //                        esto es un if un tanto raro, pero es un if

                        Yii::$app->user->identity->admin ? (
                                ['label' => 'Administración', 'url' => ['users/usersindex']]
                                ) : (
                                '<li>'

                                ),
                        Yii::$app->user->isGuest ? (
                                ['label' => '<i id="user" class="glyphicon glyphicon-user"><p>Acceder</p></i>', 'class' => 'login', 'url' => ['/site/login']]
                                ) : (
                                '<li>'
                                . Html::beginForm(['/site/logout'], 'post', ['id' => 'formularioo'])
                                . Html::submitButton(
                                        'LOGOUT (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</li>'
                                )
                    ],
                ]);

//            echo Html::img('@web/img/mtn-dew.png', ['id' => 'mtn']);


                NavBar::end();
            } else {

                echo Nav::widget([
                    'encodeLabels' => false,
                    'options' => ['id' => 'list', 'class' => 'navbar-nav navbar-left'],
                    'items' => [
                    ['label' => 'equipo',
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => 'Roster', 'url' => ['jugadores/roster'],['target'=>'_blank']],
                            ['label' => 'Estadísticas Jugadores', 'url' => ['jugadores/estadisticasuser']],
                            ['label' => 'Estadísticas Globales', 'url' => ['partidos/maximos']],
                            ['label' => 'Entrenadores', 'url' => ['entrenadores/entrenadoresuser']]
                           
                        ],
                    ],
                    ['label' => 'calendario',
                        'url' => ['contratos/contratos'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => 'Partidos 20-21', 'url' => ['partidos/resultadosuser']],
                            ['label' => 'Calendario 20-21', 'url' => ['partidos/resultadospormesuser']],
                        ],
                    ],
                    ['label' => 'Tienda',
                        'url' => 'https://www.cavaliersteamshop.com/?utm_source=Cavs&utm_medium=link&utm_campaign=cavs_nav&utm_content=homepage',
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>', 'linkOptions' => ['target'=>'_blank']
                        
                    ],
                         ['label' => 'Tickets',
                        'url' => ['#'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => 'Asientos 3D', 'url' => 'https://3ddigitalvenue.com/3dmap/clients/cleveland-cavaliers/','linkOptions' => ['target'=>'_blank']],
                            ['label' => 'Opciones premium', 'url' => 'https://www.nba.com/cavaliers/premium','linkOptions' => ['target'=>'_blank']],
                             ['label' => 'Accesibilidad Info', 'url' => 'https://www.rocketmortgagefieldhouse.com/tickets/accessibility','linkOptions' => ['target'=>'_blank']],
                        ],
                    ],

                        Yii::$app->user->isGuest ? (
                                ['label' => '<i id="user" class="glyphicon glyphicon-user"><p>Acceder</p></i>', 'class' => 'login', 'url' => ['/site/login']]
                                ) : (
                                '<li>'
                                . Html::beginForm(['/site/logout'], 'post', ['id' => 'formularioo'])
                                . Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</li>'
                                )
                    ],
                ]);

//            echo Html::img('@web/img/mtn-dew.png', ['id' => 'mtn']);


                NavBar::end();
            }
            ?>

            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Estadisticas Equipo</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Jugadores</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Entrenamientos</a>
                </li>
            </ul>
            <!--           <div class="container-fluid">-->
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <?= Alert::widget() ?>
            <?= $content ?>

            <!--            </div>-->
        </div>


        <div id="footer">
            <!--            <div id="edificios" class="container">
                        <img src="../../web/img/cielo2.png" alt=""/>
                        </div>-->
            <div id="footer1" >

                <div class="container">

                    <div id="text-footer1" class="col-md-2 containerf">

                        <h4>INFORMACIÓN ADICIONAL</h4>

                        <p style="color: #cccccc">
                            <a href="https://www.rocketmortgagefieldhouse.com/guest-experience/accessibility" title="Accessibility" style="color: white">Accesibilidad</a><br>
                            <a href="https://www.rocketmortgagefieldhouse.com/plan-your-visit/directions-parking" title="Directions to Rocket Mortgage FieldHouse" style="color: white">Direcciones</a><br>
                            <a href="https://www.nba.com/cavaliers/contact" title="Contact Us" style="color: white">¿Tienes una gran idea?</a><br>
                            <a href="https://www.teamworkonline.com/basketball-jobs/cleveland-cavaliers/cleveland-cavaliers-jobs" title="Job Opportunities" style="color: white">Empleo</a><br>
                            <a href="https://www.rocketmortgagefieldhouse.com/" title="Rocket Mortgage FieldHouse" style="color: white">Rocket Mortgage FieldHouse</a><br>

                        </p>
                    </div>

                    <div id="img-footer1" class="col-md-10 containerf">
                        <?= Html::img('@web/img/rocket.png', ['id' => 'rocket']); ?>
                        <?= Html::img('@web/img/charge.png', ['id' => 'rocket']); ?>
                        <?= Html::img('@web/img/legion.png', ['id' => 'rocket']); ?>
                        <?= Html::img('@web/img/monsters.png', ['id' => 'rocket']); ?>
                        <?= Html::img('@web/img/seat.png', ['id' => 'rocket']); ?>
                    </div>
                </div>
            </div>


            <div id="footer2">
                <div class="container">

                    <p style="color: #cccccc">
                        Copyright © 2021 NBA Media Ventures, LLC. All rights reserved. No portion of
                        NBA.com may be duplicated,redistributed or manipulated in any form. This
                        site is operated jointly by NBA and WarnerMedia. To opt out of the sale of your
                        personal information as permitted by the California Consumer Privacy Act,
                        please use the links below to visit each company’s privacy center. If you make
                        a request through the WarnerMedia Privacy Center, it will apply to data controlled
                        jointly by the NBA and WarnerMedia as well as other data controlled by WarnerMedia.
                        If you make a request through the NBA Privacy Center, it will apply to data 
                        controlled independently by the NBA.<br>
                    </p>

                    <div id="enlaces">
                        <a href="https://adspecs.nba.com/" style="color: #cccccc">Anúnciese en NBA.com</a>&nbsp;&nbsp;&nbsp;&nbsp;

                        <a href=" https://support.watch.nba.com/hc/en-us" style="color: #cccccc">Centro de Apoyo</a>&nbsp;&nbsp;&nbsp;&nbsp;


                        <a href=" https://careers.nba.com/" style="color: #cccccc">Carreras NBA</a>
                    </div>  
                </div>
            </div>
        </div>










        <?php $this->endBody() ?>

<!--        <script>
            $('#list li').hover(function () {

                $(this).children('ul').fadeToggle(10);
            });

        </script>-->

    </body>
</html>
<?php $this->endPage() ?>