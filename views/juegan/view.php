<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = $model->cod_juegan;
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="juegan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cod_juegan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cod_juegan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_juegan',
            'cod_jugador',
            'cod_partido',
            'puntos_jugador',
            'asistencias_jugador',
            'rebotes_jugador',
            'robos',
            'tiros_jugador',
            'aciertos_jugador',
            't3_intentados',
            't3_acertados',
            'tl_intentados',
            'tl_acertados',
            'minutos_jugador',
            'tapones',
            '+/-',
        ],
    ]) ?>

</div>
