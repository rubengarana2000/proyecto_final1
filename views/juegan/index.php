<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Juegans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Juegan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

           
            'cod_jugador',
            'cod_partido',
            'puntos_jugador',
            'asistencias_jugador',
            'rebotes_jugador',
            'robos',
            'tiros_jugador',
            'aciertos_jugador',
            't3_intentados',
            't3_acertados',
            'tl_intentados',
            'tl_acertados',
            'minutos_jugador',
            //'tapones',
            //'+/-',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
    </div>
