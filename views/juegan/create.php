<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = 'Create Juegan';
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegan-create">

    <h1></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'cod'=>$cod,
       
        
    ]) ?>

</div>
