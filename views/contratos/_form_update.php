<?php

use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Jugadores;
use app\models\Entrenamientos;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <div class="container">
        <h1 id="tituloform">CONTRATOS</h1>


        <?php
        $jugadores = Jugadores::find()
                ->leftJoin("contratos", "jugadores.cod_jugador=contratos.cod_jugador")
//                  ->where('agente_libre=0')
//                  ->andWhere("estado=1")
                ->andWhere("año1 is  null")
                ->all();

        foreach ($jugadores as &$jugador) {
            $jugador->nombre = $jugador->nombre . ' ' . $jugador->apellidos;
        }
        $items = ArrayHelper::map($jugadores, 'cod_jugador', 'nombre');


        $codigo = $model->cod_jugador;
//        para coger el nombre del jugador en especifico
        $jugador1 = Jugadores::find()
                ->select('nombre,apellidos')
                ->where('cod_jugador=' . $codigo)
                ->one();



        $array = [
            ['id' => $model->cod_jugador, 'data' => $jugador1->nombre . ' ' . $jugador1->apellidos],
        ];
        $result = ArrayHelper::map($array, 'id', 'data');


        $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'cod_jugador' => ['label' => 'Jugador', 'type' => Form::INPUT_DROPDOWN_LIST, 'items' => $result, 'options' => ['disabled' => true]],
                'clausula_antitraspaso' => ['label' => 'Cláusula antitraspaso', 'type' => Form::INPUT_RADIO_LIST, 'items' => [true => 'sí', false => 'no'], 'options' => ['inline' => false]],
                'opcion_jugador' => ['label' => 'Opción de jugador', 'type' => Form::INPUT_RADIO_LIST, 'items' => [true => 'sí', false => 'no'], 'options' => ['inline' => false]],
            ]
        ]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'año1' => ['label' => 'Salario Año 1', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Salario del jugador en el primer año']],
                'año2' => ['label' => 'Salario Año 2', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Salario del jugador en el segundo año']],
                'año3' => ['label' => 'Salario Año 3', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Salario del jugador en el tercer año']],
            ]
        ]);
     

        echo $form->field($model, 'fecha_inicio')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy',
   
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
    
     'widgetOptions' => [
        'pluginOptions' => [
            
            'autoclose' => true,
            
        ]
    ],
    'language' => 'es'
]);

       

            echo $form->field($model, 'fecha_fin')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy',
   
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
    
     'widgetOptions' => [
        'pluginOptions' => [
            
            'autoclose' => true,
            
        ]
    ],
    'language' => 'es'
]);
        echo '<br>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                '' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<div style="  text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color: #EDEDED;margin-left: -30px;">' .
                    Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                    '</div>'
                ],
            ]
        ]);
        ?>


    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
