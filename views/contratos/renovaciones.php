<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$this->title = 'CONTRATOS';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
         <?php
        if (Yii::$app->user->identity->admin) {
            ?>
        
        <h1 id="titulorenovaciones">  <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'value' => function ($model) {
                        return number_format($model->año1) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'value' => function ($model) {
                        return number_format($model->año2) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'value' => function ($model) {
                        return number_format($model->año3) . ' $';
                    }
                ],
                    [
                    'label' => 'cláusula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{renovar}',
                    'buttons' => [
                        'renovar' => function ($url, $model) {
                            $url = Url::to(['contratos/update', 'id' => $model->cod_jugador]);
                            return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', $url, ['title' => 'Renovar']);
                        },
                    ],
                ],
            ]
        ]);?>Renovaciones</h1>

        <?=
     GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'value' => function ($model) {
                        return number_format($model->año1) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'value' => function ($model) {
                        return number_format($model->año2) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'value' => function ($model) {
                        return number_format($model->año3) . ' $';
                    }
                ],
                  [
                    'label' => 'cláusula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{renovar}',
                    'buttons' => [
                        'renovar' => function ($url, $model) {
//                            $url = Url::to(['contratos/update', 'id' => $model->cod_jugador]);
                            return Html::a('<span class="glyphicon glyphicon-briefcase"></span>',['contratos/update', 'id' => $model->cod_jugador], ['title' => 'Renovar']);
                          
                        },
                    ],
                ],
            ]
        ]);
        ?>
           <?php
        } else {
            ?>
<h1 id="titulorenovaciones">  <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'value' => function ($model) {
                        return number_format($model->año1) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'value' => function ($model) {
                        return number_format($model->año2) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'value' => function ($model) {
                        return number_format($model->año3) . ' $';
                    }
                ],
                    [
                    'label' => 'clausula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{renovar}',
                    'buttons' => [
                        'renovar' => function ($url, $model) {
                            $url = Url::to(['contratos/update', 'id' => $model->cod_jugador]);
                            return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', $url, ['title' => 'Renovar']);
                        },
                    ],
                ],
            ]
        ]);?>Renovaciones</h1>

        <?=
     GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'value' => function ($model) {
                        return number_format($model->año1) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'value' => function ($model) {
                        return number_format($model->año2) . ' $';
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'value' => function ($model) {
                        return number_format($model->año3) . ' $';
                    }
                ],
                  [
                    'label' => 'clausula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
              
            ]
        ]);
                 }
        ?>
 
    </div>
</div>

<script>
    $('table td:first-child').css('text-align', 'initial')

</script>

