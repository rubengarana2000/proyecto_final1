<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use bsadnu\googlecharts\ColumnChart;
use bsadnu\googlecharts\ComboChart;
use kartik\alert\Alert;
use kartik\alert\AlertBlock;

$this->title = 'MARGEN SALARIAL';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<script src="../js/jquery.js"></script>
<div class="container">
    <?php
    $impuesto = $margen->query - 132627000;
    $multa = 0;
    switch ($impuesto) {
        case($impuesto > 0 and $impuesto < 5000000);
            $multa = number_format($impuesto * 1.5);
            break;
        case($impuesto > 5000000 and $impuesto < 10000000);
            $multa = number_format($impuesto * 1.75);
            break;
        case ($impuesto > 10000000 and $impuesto < 15000000);
            $multa = number_format($impuesto * 2.50);
            break;
        case ($impuesto > 15000000 and $impuesto < 20000000);
            $multa = number_format($impuesto * 3.25);
            break;
        case ($impuesto > 20000000);
            $multa = number_format($impuesto * 3.75);
        default :"defaulkt";
    }
    ?> 
    <?php
    if ($multa != 0) {
        echo Alert::widget([
            'type' => Alert::TYPE_DANGER,
            'title' => '   ¡CUIDADO!   ',
            'icon' => 'fas fa-exclamation-circle',
            'body' => 'En esta situación se tendría que pagar una multa de $ ' . $multa,
            'showSeparator' => true,
        ]);
    }
    ?>
</div>


<div class="jugadores-index">
    <div class="container">


        <h1 id="titulosinwarning"><?= Html::encode($this->title) ?></h1>
        <div id="statscontrato" class="row">

            <div id="contratocaro" class="col-lg-6">
                <h1>Salario más elevado</h1>
                <div>
                    <div class="col-lg-6">
                        <ul>

                            <li id="titulocontratocaro">
                                <?php
                                echo number_format($margen6->query);
                                ?>
                            </li>
                            <li><?=$margen4->query.' '.$margen9->query?></li>
                            <li>2020-2021 Temporada Regular</li>

                        </ul>
                    </div>
                    <div id="foto" class="col-lg-6">
                        <?php
//   PARA COGER EL CODIGO DE CADA JUGADOR Y ASI COGER SU IMAGEN


                        echo Html::img('@web/img/' . $margen4->query . '2.png');
                        ?>

                        <!--       PARA PASAR A LA ACCION JUEGAN/CREATE EL COD_JUGADOR Y EL COD DEL PARTIDO ESPECIFICO-->

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <h1 id="margen">Cantidad económica gastada actualmente:</h1>

                <p id='submargen'> <?php echo '$ ' . number_format(($margen->query)); ?> </p>

                <h1 id="margen">Cantidad económica disponible actualmente:</h1>

                <p id='submargen'> <?php echo number_format(($margen2->query)); ?> </p>

                <h1 id="margen">Cantidad económica disponible hasta impuesto de lujo:</h1>

                <p id='submargen'> <?php echo number_format(($margen3->query)); ?> </p>
            </div>

        </div>


        <h1 id="titulosin2">Grafico Salarial</h1>
        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
            <?=
            ComboChart::widget([
                'id' => 'my-column-chart-id',
                'data' => [
                    ['Year', 'Cantidad Gastada', 'Salario Máximo', 'Impuesto de Lujo'],
                    ['2020/2021', (int) $margen->query, 109140000, 132627000],
                    ['2021/2022', (int) $margen7->query, 109140000, 132627000],
                    ['2022/2023', (int) $margen8->query, 109140000, 142627000],
                ],
                'options' => [
                    'fontName' => 'Verdana',
                    'height' => 400,
                    'fontSize' => 9.9,
                    'chartArea' => [
                        'left' => '5%',
                        'width' => '100%',
                        'height' => 300,
                    ],
                    'colors' => ['#041E42', '#FDBB30', '#5f022a'],
                    'isStacked' => true,
                    'tooltip' => [
                        'textStyle' => [
                            'fontName' => 'Verdana',
                            'fontSize' => 13
                        ]
                    ],
                    'hAxis' => [
                        'gridlines' => [
                            'color' => '#e5e5e5',
                            'count' => 10
                        ],
                        'minValue' => 0
                    ],
                    'seriesType' => 'bars',
                    'series' => [
                        1 => [
                            'type' => 'line',
                            'pointSize' => 5
                        ],
                        2 => [
                            'type' => 'line',
                            'pointSize' => 5
                        ],
                    ],
                
                  
                    'legend' => [
                        'position' => 'top',
                        'alignment' => 'center',
                        'textStyle' => [
                            'fontSize' => 14]
                    ]
                ]
            ]);
            ?>
        </div>
    </div>
</div>


<link href="dist/main.css" rel="stylesheet" type="text/css" />

<script>
    var numeroentero = $(".col-lg-6 > p:nth-child(4)").text();
    var numeroentero2 = ($(".col-lg-6 > p:nth-child(6)").text());
    var numero = parseInt($(".col-lg-6 > p:nth-child(4)").text());
    var numero2 = parseInt($(".col-lg-6 > p:nth-child(6)").text());

    if (numero > 0) {

        $(".col-lg-6 > p:nth-child(4)").html("<p style='color:green;'>$" + numeroentero + "</p>");

    } else {

        $(".col-lg-6 > p:nth-child(4)").html("<p style='color:red;'>$" + numeroentero + "</p>");
    }
    if (numero2 > 0) {

        $(".col-lg-6 > p:nth-child(6)").html("<p style='color:green;'>$" + numeroentero2 + "</p>");

    } else {

        $(".col-lg-6 > p:nth-child(6)").html("<p style='color:red;'>$" + numeroentero2 + "</p>");
    }
</script>

