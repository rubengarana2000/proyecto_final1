<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contratos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contratos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Contratos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_contrato',
            'año1',
            'año2',
            'opcion_jugador',
            'fecha_inicio',
            //'fecha_fin',
            //'clausula_antitraspaso',
            //'cod_jugador',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
