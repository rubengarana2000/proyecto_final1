<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */

$this->title = 'Update Contratos: ' . $model->cod_contrato;
$this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_contrato, 'url' => ['view', 'id' => $model->cod_contrato]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contratos-update">

    <h1></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
