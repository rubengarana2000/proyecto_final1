<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Involucran */

$this->title = 'Update Involucran: ' . $model->cod_involucran;
$this->params['breadcrumbs'][] = ['label' => 'Involucrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_involucran, 'url' => ['view', 'id' => $model->cod_involucran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="involucran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
