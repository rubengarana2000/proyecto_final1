<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InvolucranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="involucran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cod_involucran') ?>

    <?= $form->field($model, 'cod_jugador') ?>

    <?= $form->field($model, 'cod_traspaso') ?>

    <?= $form->field($model, 'equipo_inicial') ?>

    <?= $form->field($model, 'equipo_final') ?>

    <?php // echo $form->field($model, 'imagen') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
