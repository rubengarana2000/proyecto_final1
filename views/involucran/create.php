<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Involucran */

$this->title = 'Create Involucran';
$this->params['breadcrumbs'][] = ['label' => 'Involucrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="involucran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
