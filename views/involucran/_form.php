<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Involucran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="involucran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_jugador')->textInput() ?>

    <?= $form->field($model, 'cod_traspaso')->textInput() ?>

    <?= $form->field($model, 'equipo_inicial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'equipo_final')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagen')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
