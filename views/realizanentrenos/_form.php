<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanEntrenos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realizan-entrenos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_realizan')->textInput() ?>

    <?= $form->field($model, 'cod_jugador')->textInput() ?>

    <?= $form->field($model, 'cod_entrenamiento')->textInput() ?>

    <?= $form->field($model, 'distancia_jugador')->textInput() ?>

    <?= $form->field($model, 'calorias_jugador')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
