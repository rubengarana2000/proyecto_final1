<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RealizanEntrenosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Realizan Entrenos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realizan-entrenos-index">
    <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Realizan Entrenos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_realizan',
            'cod_jugador',
            'cod_entrenamiento',
            'distancia_jugador',
            'calorias_jugador',
            //'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
    </div>
