<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use kartik\icons\Icon;
Icon::map($this);
$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = ['label' => 'entrenadores', 'url' => ['consulta2']];

?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
      
        <h1 id="titulo"><?=
            ExportMenu::widget([
                'dataProvider' => $resultados,
                'columns' => [
                   [
                    'attribute' => 'username',
                    'label' => 'usuario',
                    'value' => function ($model) {
                        return $model->username;
                    }
                ],
                    [
                    'label' => 'Administrador',
                    'attribute' => 'admin',
                    'value' => function ($model) {
                        if ($model->admin == 0) {
                            return 'NO';
                        } else {
                            return 'SÍ';
                        }
                    }
                ],
                                
                    ['class' => 'yii\grid\ActionColumn',
                    'template' => '{renovar}{delete}',
                    'buttons' => [
                        'renovar' => function ($url, $model) {
//                            $url = Url::to(['contratos/update', 'id' => $model->cod_jugador]);
                            return Html::a('<span class="fas fa-user-shield"></span>',['users/privilegios', 'id' => $model->id], ['title' => 'Dar/Quitar Privilegios']);
                          
                        },
                    ],
                ],
                ],
            ]);?>Usuarios<?= Html::a('Añadir usuario', ['site/register'], ['class' => 'btn']) ?></h1>


    
            <?=
        
             GridView::widget([
                'dataProvider' => $resultados,
                'columns' => [
                   [
                    'attribute' => 'username',
                    'label' => 'usuario',
                    'value' => function ($model) {
                        return $model->username;
                    }
                ],
                    [
                    'label' => 'Administrador',
                    'attribute' => 'admin',
                    'value' => function ($model) {
                        if ($model->admin == 0) {
                            return 'NO';
                        } else {
                            return 'SÍ';
                        }
                    }
                ],
                                
                    ['class' => 'yii\grid\ActionColumn',
                    'template' => '{renovar}{delete}',
                    'buttons' => [
                        'renovar' => function ($url, $model) {
//                            $url = Url::to(['contratos/update', 'id' => $model->cod_jugador]);
                            return Html::a('<span class="fas fa-user-shield"></span>',['users/privilegios', 'id' => $model->id], ['title' => 'Dar/Quitar Privilegios']);
                          
                        },
                    ],
                ],
                ],
            ]);
            ?>
      

    </div>
</div>

