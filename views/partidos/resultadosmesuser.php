<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use aryelds\sweetalert\SweetAlert;
use kartik\export\ExportMenu;

$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,
text: "Esta acción será definitiva",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Borrar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'PARTIDOS';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
        <?php
        $resta=1;
        $suma=1;
        $mestitulo;
       switch ($mes) {
                                    case '1' :
                                        $mestitulo='enero';
                                        $resta=-11;
                                        break;
                                    case '2' :
                                         $mestitulo='febrero';
                                        break;
                                    case '3' :
                                         $mestitulo='marzo';
                                        break;
                                    case '4' :
                                         $mestitulo='abril';
                                        break;
                                    case '5' :
                                         $mestitulo='mayo';
                                        break;
                                    case '6' :
                                         $mestitulo='junio';
                                        break;
                                    case '7' :
                                         $mestitulo='julio';
                                        break;
                                    case '8' :
                                         $mestitulo='agosto';
                                        break;
                                    case '9' :
                                         $mestitulo='septiembre';
                                        break;
                                    case '10' :
                                         $mestitulo='octubre';
                                        break;
                                    case '11' :
                                         $mestitulo='noviembre';
                                        break;
                                    case '12' :
                                         $mestitulo='diciembre';
                                        $suma=-11;
                                        break;
      
}?>
            <h1 id="titulonoadmincalendaruser">
<?= Html::a('<', ['resultadospormesuser', 'mes' => $mes - $resta], ['class' => 'btn',"id"=>"bajar"]) ?><?= Html::encode($mestitulo) ?><?= Html::a('>', ['resultadospormesuser', 'mes' => $mes + $suma], ['class' => 'btn',"id"=>"subir"]) ?></h1>




            <?=
            GridView::widget([
                'dataProvider' => $resultados,
                //usamos esto para poder concatenar las columnas.
                'columns' => [
                    'resultado',
                    [
                        'label' => 'puntos',
                        'value' => function ($model) {
                            return $model->nosotros . '-' . $model->puntos_rivales;
                        }
                    ],
                    [
                        'attribute' => 'nombre_rival',
                        'value' => function ($model) {
                            return Html::a($model->nombre_rival, ['partidos/vistaindividual', 'cod' => $model->cod_partido]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'estadio',
                        'value' => function ($model) {
                            switch ($model->estadio) {
                                case 'Atlanta Hawks' :
                                    return 'State Farm Arena';
                                    break;
                                case 'Boston Celtics' :
                                    return 'TD Garden';
                                    break;
                                case 'Brooklyn Nets' :
                                    return 'Barclays Center';
                                    break;
                                case 'Charlotte Hornets' :
                                    return 'Spectrum Center';
                                    break;
                                case 'Chicago Bulls' :
                                    return 'United Center';
                                    break;
                                case 'Dallas Mavericks' :
                                    return 'American Airlines Center';
                                    break;
                                case 'Denver Nuggets' :
                                    return 'Pepsi Center';
                                    break;
                                case 'Detroit Pistons' :
                                    return 'Little Caesars Arena';
                                    break;
                                case 'Golden State Warriors' :
                                    return 'Chase Center';
                                    break;
                                case 'Houston Rockets' :
                                    return 'Toyota Center';
                                    break;
                                case 'Indiana Pacers' :
                                    return 'Bankers Life Fieldhouse';
                                    break;
                                case 'Los Angeles Clippers' :
                                    return 'Staples Center';
                                    break;
                                case 'Los Angeles Lakers' :
                                    return 'Staples Center';
                                    break;
                                case 'Memphis Grizzlies' :
                                    return 'FedEx Forum';
                                    break;
                                case 'Miami Heat' :
                                    return 'American Airlines Arena';
                                    break;
                                case 'Milwaukee Bucks' :
                                    return 'Fiserv Forum';
                                    break;
                                case 'Minnesota Timberwolves' :
                                    return 'Target Center';
                                    break;
                                case 'New Orleans Pelicans' :
                                    return 'Smoothie King Center';
                                    break;
                                case 'New York Knicks' :
                                    return 'Madison Square Garden';
                                    break;
                                case 'Oklahoma City Thunder' :
                                    return 'Chesapeake Energy Arena';
                                    break;
                                case 'Orlando Magic' :
                                    return 'Amway Center';
                                    break;
                                case 'Philadelphia 76ers' :
                                    return 'Wells Fargo Center';
                                    break;
                                case 'Phoenix Suns' :
                                    return 'Talking Stick Resort Arena';
                                    break;
                                case 'Portland Trail Blazers' :
                                    return 'Moda Center';
                                    break;
                                case 'Sacramento Kings' :
                                    return 'Golden 1 Center';
                                    break;
                                case 'San Antonio Spurs' :
                                    return 'AT&T Center';
                                    break;
                                case 'Toronto Raptors' :
                                    return 'Scotiabank Arena';
                                    break;
                                case 'Utah Jazz' :
                                    return 'Vivint Smart Home Arena';
                                    break;
                                case 'Washington Wizards' :
                                    return 'Capital One Arena';
                                    break;
                                default:
                                    return 'Rocket Mortage FieldHouse';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                $url = Url::to(['partidos/vistaindividualuser', 'cod' => $model->cod_partido]);
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'ver estadisticas individuales']);
                            },
                        ],
                    ],
                ],
            ]);
        
        ?>


    </div>

</div>


<script>

    $("table td:first-child").each(function () {
        var texto = $(this).text();
        if (texto == 1) {

            $(this).text('VICTORIA').css('color', 'green');
        }
        if (texto == 0) {
            $(this).text('DERROTA').css('color', 'red')
        }




    });
</script>

