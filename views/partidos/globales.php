<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use kartik\export\ExportMenu;

$this->title = 'Globales';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">

        <!--   <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#">Estadisticas Equipo</a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="../jugadores/estadisticas">Jugadores</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="../realizan/entrenos">Entrenamientos</a>
          </li>
        </ul>-->
        
        <?= Html::a('Totales', ['maximos'], ['class' => 'btn', 'id' => 'boton1']) ?>
        <?= Html::a('Líderes', ['lideres'], ['class' => 'btn', 'id' => 'boton2']) ?>

 <?php
if (!Yii::$app->user->isGuest) {?>
        <h1 id="titulo2"><?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            'columns' => [
                'fecha',
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
              switch ($model->fechames) {
                            case '1' :
                                return 'enero';
                                break;
                            case '2' :
                                return 'febrero';
                                break;
                            case '3' :
                                return 'marzo';
                                break;
                            case '4' :
                                return 'abril';
                                break;
                            case '5' :
                                return 'mayo';
                                break;
                            case '6' :
                                return 'junio';
                                break;
                            case '7' :
                                return 'julio';
                                break;
                            case '8' :
                                return 'agosto';
                                break;
                            case '9' :
                                return 'septiembre';
                                break;
                            case '10' :
                                return 'octubre';
                                break;
                            case '11' :
                                return 'noviembre';
                                break;
                            case '12' :
                                return 'diciembre';
                                break;
                           
                           
                        }
                       
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);?>GLOBALES POR MES</h1>



        <!--poder usar mas grindviews en una misma vista:-->


        <?=
        
         GridView::widget([
            'dataProvider' => $resultados,
            'columns' => [
                 [
                    'label' => 'MES',
                    'attribute' => 'fechames',
                    'value' => function ($model) {
              switch ($model->fechames) {
                            case '1' :
                                return 'Enero';
                                break;
                            case '2' :
                                return 'Febrero';
                                break;
                            case '3' :
                                return 'Marzo';
                                break;
                            case '4' :
                                return 'Abril';
                                break;
                            case '5' :
                                return 'Mayo';
                                break;
                            case '6' :
                                return 'Junio';
                                break;
                            case '7' :
                                return 'Julio';
                                break;
                            case '8' :
                                return 'Agosto';
                                break;
                            case '9' :
                                return 'Septiembre';
                                break;
                            case '10' :
                                return 'Octubre';
                                break;
                            case '11' :
                                return 'Noviembre';
                                break;
                            case '12' :
                                return 'Diciembre';
                                break;
                           
                           
                        }
                       
                    }
                ],
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);
        ?>

        <h1 id="titulo2"> <?=
        ExportMenu::widget([
            'dataProvider' => $resultados6,
            'columns' => [
                'nombre_rival',
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);?> GLOBALES POR EQUIPOS</h1>
        <?=
       GridView::widget([
            'dataProvider' => $resultados6,
            'columns' => [
                'nombre_rival',
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);
        ?>
          <?php
        } else {
            ?>
        
<h1 id="titulo2">GLOBALES POR MES</h1>



        <!--poder usar mas grindviews en una misma vista:-->


        <?=
        
         GridView::widget([
            'dataProvider' => $resultados,
            'columns' => [
                 [
                    'label' => 'MES',
                    'attribute' => 'fechames',
                    'value' => function ($model) {
              switch ($model->fechames) {
                            case '1' :
                                return 'Enero';
                                break;
                            case '2' :
                                return 'Febrero';
                                break;
                            case '3' :
                                return 'Marzo';
                                break;
                            case '4' :
                                return 'Abril';
                                break;
                            case '5' :
                                return 'Mayo';
                                break;
                            case '6' :
                                return 'Junio';
                                break;
                            case '7' :
                                return 'Julio';
                                break;
                            case '8' :
                                return 'Agosto';
                                break;
                            case '9' :
                                return 'Septiembre';
                                break;
                            case '10' :
                                return 'Octubre';
                                break;
                            case '11' :
                                return 'Noviembre';
                                break;
                            case '12' :
                                return 'Diciembre';
                                break;
                           
                           
                        }
                       
                    }
                ],
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);
        ?>

        <h1 id="titulo2"> GLOBALES POR EQUIPOS</h1>
        <?=
       GridView::widget([
            'dataProvider' => $resultados6,
            'columns' => [
                'nombre_rival',
                'puntos',
                [
                    'label' => 'TC%',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3%',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL%',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
            ],
        ]);
        ?>


    </div>
</div>
<?php
}
?>