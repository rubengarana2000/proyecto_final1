<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */

$this->title = 'Create Partidos';
$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-create">

    <h1></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'codigo'=>$model->cod_partido,
      
    ]) ?>

</div>
