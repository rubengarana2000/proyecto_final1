<?php

use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Jugadores;
use app\models\Entrenamientos;
use yii\web\View;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="/{webroot}/js/jquery.js"></script>
<div class="form">
    <div class="container">

        <h1 id="tituloform">PARTIDOS</h1>

        <?php
        $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'nombre_rival' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => [
                        'Atlanta Hawks' => 'Atlanta Hawks',
                        'Boston Celtics' => 'Boston Celtics',
                        'Brooklyn Nets' => 'Brooklyn Nets',
                        'Charlotte Hornets' => 'Charlotte Hornets',
                        'Chicago Bulls' => 'Chicago Bulls',
                        'Dallas Mavericks' => 'DAtlanta Hawksallas Mavericks',
                        'Denver Nuggets' => 'Denver Nuggets',
                        'Detroit Pistons' => 'Detroit Pistons',
                        'Golden State Warriors' => 'Golden State Warriors',
                        'Houston Rockets' => 'Houston Rockets',
                        'Indiana Pacers' => 'Indiana Pacers',
                        'Los Angeles Clippers' => 'Los Angeles Clippers',
                        'Los Angeles Lakers' => 'Los Angeles Lakers',
                        'Memphis Grizzlies' => 'Memphis Grizzlies',
                        'Miami Heat' => 'Miami Heat',
                        'Milwaukee Bucks' => 'Milwaukee Bucks',
                        'Minnesota Timberwolves' => 'Minnesota Timberwolves',
                        'New Orleans Pelicans' => 'New Orleans Pelicans',
                        'New York Knicks' => 'New York Knicks',
                        'Oklahoma City Thunder' => 'Oklahoma City Thunder',
                        'Orlando Magic' => 'Orlando Magic',
                        'Philadelphia 76ers' => 'Philadelphia 76ers',
                        'Phoenix Suns' => 'Phoenix Suns',
                        'Portland Trail Blazers' => 'Portland Trail Blazers',
                        'Sacramento Kings' => 'Sacramento Kings',
                        'San Antonio Spurs' => 'San Antonio Spurs',
                        'Toronto Raptors' => 'Toronto Raptors',
                        'Utah Jazz' => 'Utah Jazz',
                        'Washington Wizards' => 'Washington Wizards'],
                    'options' => ['prompt'=>'Selecione un rival']],
                'estadio' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => [
                        'Rockect Mortage Field House' => 'Rockect Mortage Field House',
                        'Atlanta Hawks' => 'State Farm Arena',
                        'Boston Celtics' => 'TD Garden',
                        'Brooklyn Nets' => 'Barclays Center',
                        'Charlotte Hornets' => 'Spectrum Center',
                        'Chicago Bulls' => 'United Center',
                        'Dallas Mavericks' => 'American Airlines Center',
                        'Denver Nuggets' => 'Pepsi Center',
                        'Detroit Pistons' => 'Little Caesars Arena',
                        'Golden State Warriors' => 'Chase Center',
                        'Houston Rockets' => 'Toyota Center',
                        'Indiana Pacers' => 'Bankers Life Fieldhouse',
                        'Los Angeles Clippers' => 'Staples Center',
                        'Los Angeles Lakers' => 'Staples Center',
                        'Memphis Grizzlies' => 'FedEx Forum',
                        'Miami Heat' => 'American Airlines Arena',
                        'Milwaukee Bucks' => 'Fiserv Forum',
                        'Minnesota Timberwolves' => 'Target Center',
                        'New Orleans Pelicans' => 'Smoothie King Center',
                        'New York Knicks' => 'Madison Square Garden',
                        'Oklahoma City Thunder' => 'Chesapeake Energy Arena',
                        'Orlando Magic' => 'Amway Center',
                        'Philadelphia 76ers' => 'Wells Fargo Center',
                        'Phoenix Suns' => 'Talking Stick Resort Arena',
                        'Portland Trail Blazers' => 'Moda Center',
                        'Sacramento Kings' => 'Golden 1 Center',
                        'San Antonio Spurs' => 'AT&T Center',
                        'Toronto Raptors' => 'Scotiabank Arena',
                        'Utah Jazz' => 'Vivint Smart Home Arena',
                        'Washington Wizards' => 'Capital One Arena',],
                    'options' => ['placeholder' => 'Puntos anotados por el equipo rival'],
                ]
            ]
        ]);
       
               echo $form->field($model, 'fecha')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
      
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy',
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
   
     'widgetOptions' => [
        'pluginOptions' => [
          
            'autoclose' => true,
            
        ]
    ],
    
    'language' => 'es'
]); 
        echo '<br>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'puntos_rivales' => ['label' => 'Puntos rival', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Nombre del equipo rival']],

          ] 
        ]);
        echo $form->field($model, 'video')->fileInput()->label('Vídeo del partido');
         echo '<br>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                '' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<div style="  text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color: #EDEDED;margin-left: -30px;">' .
                    Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                    '</div>'
                ],
            ]
        ]);




        
        ?>


        <?php ActiveForm::end(); ?>






    </div>
</div>

<?php 
$script = <<< JS

$('#partidos-nombre_rival').change(function(){
        $('#partidos-estadio option').hide();
        $('#partidos-estadio option[value="'+$(this).val()+'"]').show();
   
         $('#partidos-estadio option[value="Rockect Mortage Field House"]').show();
     });

JS;
$this->registerJs($script, View::POS_END);
?>
