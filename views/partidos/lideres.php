<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Globales';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">

        
     <?= Html::a('Totales', ['maximos'], ['class' => 'btn','id'=>'boton1v2']) ?>
        <?= Html::a('Líderes', ['lideres'], ['class' => 'btn','id'=>'boton2v2']) ?>
<h1 id="titulo2">MÁXIMOS DE EQUIPO</h1>
        <div class="row">
            <div id="card1" class="col-lg-6">


                <!-- HACEMOS EL LISTVIEW EN EL QUE LE PASAMOS EL CODIGO DEOL PARTIDO0 EN ESPECIFICO-->

                <?=
                ListView::widget([
                    'dataProvider' => $resultados2,
                    'itemView' => '_globales',
                ]);
                ?>

                <div id="card2" class="col-lg-6">
                    <?=
                    ListView::widget([
                        'dataProvider' => $resultados3,
                        'itemView' => '_globales2',
                    ]);
                    ?>
                </div>
                <div class="row">
                 <div id="card1" class="col-lg-6">


                <!-- HACEMOS EL LISTVIEW EN EL QUE LE PASAMOS EL CODIGO DEOL PARTIDO0 EN ESPECIFICO-->

                <?=
                ListView::widget([
                    'dataProvider' => $resultados4,
                    'itemView' => '_globales3',
                ]);
                ?>

                <div id="card2" class="col-lg-6">
                    <?=
                    ListView::widget([
                        'dataProvider' => $resultados5,
                        'itemView' => '_globales4',
                    ]);
                    ?>
                </div>
            </div>
                    </div>
            </div>

        </div>
     </div>
         </div>