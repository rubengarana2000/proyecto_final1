<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PartidosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cod_partido') ?>

    <?= $form->field($model, 'puntos_rivales') ?>

    <?= $form->field($model, 'nombre_rival') ?>

    <?= $form->field($model, 'imagen') ?>

    <?= $form->field($model, 'estadio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
