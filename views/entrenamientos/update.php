<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenamientos */

$this->title = 'Update Entrenamientos: ' . $model->cod_entrenamiento;
$this->params['breadcrumbs'][] = ['label' => 'Entrenamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_entrenamiento, 'url' => ['view', 'id' => $model->cod_entrenamiento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenamientos-update">

    <h1></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
