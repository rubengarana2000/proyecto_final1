<?php
use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="../js/jquery.js"></script>
<div class="form">
    <div class="container">
               <h1 id="tituloform">JUGADORES</h1>
               <?php

   $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>3,
    'attributes'=>[
        'nombre'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Nombre del jugador']],
        'apellidos'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Apellidos del jugador']],
        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ]
]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'altura'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Altura del jugador cm']],
        'envergadura'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Envergadura del jugador cm']],
//        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ]
]);

echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'años_carrera'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Años de carrera del jugador'],['min'=>0,'max'=>1]],
        'lesion'=>['label'=>'Lesión', 'items'=>[0=>'sí', 1=>'no'], 'type'=>Form::INPUT_RADIO_LIST],
        
//        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ],
    
    
]);

echo $form->field($model, 'fecha_nacimiento')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy',
   
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
    
     'widgetOptions' => [
        'pluginOptions' => [
            
             'endDate'=> date('d-m-Y'),
                'startDate'=> date('d-m-Y',strtotime('-60 years')),
            'autoclose' => true,
            
        ]
    ],
    'language' => 'es'
]);
 echo '<br>';
echo $form->field($model, 'eventImage')->fileInput()->label('Imagen del jugador');

  echo '<br>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                '' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<div style="  text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color: #EDEDED;margin-left: -30px;">' .
                     Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                    '</div>'
                ],
            ]
        ]);



//echo Html::button('Submit', ['type'=>'button', 'class'=>'btn btn-primary']);
ActiveForm::end();
?>
</div>
    </div>
<script>
  



</script>