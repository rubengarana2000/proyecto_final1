<?php
use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <div class="container">
               <h1 id="tituloform">JUGADORES</h1>
               <?php

   $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'nombre'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Nombre del jugador']],
        
        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ]
]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>3,
    'attributes'=>[
        'puntos_partido'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Puntos por partido'],'required'],
        'rebotes_partido'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Rebotes por partido']],
        'asistencias_partido'=>['type'=>Form::INPUT_TEXT,'options'=>['placeholder'=>'Asistencias por partido']],

    ]
]);


echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'años'=>['label'=>'Edad','type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Años del jugador']],
        'años_carrera'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Años de carrera del jugador']],
//        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ],
    
    
]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'equipo_anterior'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Equipo Anterior']],
        'liga'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Liga']],
//        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],

    ],
    
    
]);

echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[


    
        ''=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color:#EDEDED;margin-left: -30px;;">' . 
               Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                '</div>'
        ],
        ],
]);



//echo Html::button('Submit', ['type'=>'button', 'class'=>'btn btn-primary']);
ActiveForm::end();
?>
</div>
    </div>