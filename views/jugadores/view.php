<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */

$this->title = $model->cod_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jugadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cod_jugador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cod_jugador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_jugador',
            'nombre',
            'apellidos',
            'lesion',
            'años_carrera',
            'puesto',
            'envergadura',
            'altura',
            'numero',
            'imagen',
            'puntos_partido',
            'rebotes_partido',
            'asistencias_partido',
            'agente_libre',
            'estado',
            'equipo_anterior',
            'liga',
            'años',
        ],
    ]) ?>

</div>
