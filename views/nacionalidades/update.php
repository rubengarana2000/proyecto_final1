<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidades */

$this->title = 'Update Nacionalidades: ' . $model->cod_nacionalidad;
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_nacionalidad, 'url' => ['view', 'id' => $model->cod_nacionalidad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nacionalidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
