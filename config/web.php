<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name'=>'cavaliers',
    'language'=>'es',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'P-OAFzFc4wK65zW33Sd37JbnwBfN2C16',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
       'user' => [
    'identityClass' => 'app\models\User', 
    'enableAutoLogin' => true,

],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
     
       'urlManager' => [
    'class' => 'yii\web\UrlManager',
    // Disable index.php
    'showScriptName' => false,
    // Disable r= routes
    'enablePrettyUrl' => true,
    'rules' => array(
            '<controller:\w+>/<id:\d+>' => '<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    ),
    ],
        
    ],
//    'as access' => [
//
//        'class' => \yii\filters\AccessControl::className(),//AccessControl::className(),
//
//        'rules' => [
//
//            [
//
//                'actions' => ['login', 'error'],
//
//                'allow' => true,
//
//            ],
//
//            [
//
//                'actions' => ['logout', 'index','create_1','resultados','maximos','vistaindividual','create','actualizar','contratos', 'delete','renovaciones','update','agentes','lideres','consulta2','estadisticas','renovar','entrenos','margen'], // add all actions to take guest to login page
//
//                'allow' => true,
//
//                'roles' => ['@'],
//
//            ],
//
//        ],
//
//    ],

    'modules' => [
         'datecontrol' => [
            'class' => 'kartik\datecontrol\Module',
        
            
        ],
     'gridview' => ['class' => 'kartik\grid\Module']
],
       
    
    'params' => $params,
    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
