/**
 * Override the default yii confirm dialog. This function is 
 * called by yii when a confirmation is requested.
 *
 * @param string message the message to display
 * @param string ok callback triggered when confirmation is true
 * @param string cancelCallback callback triggered when cancelled
 */
yii.confirm = function (message, okCallback, cancelCallback) {
   
swal({
title: message,
text: "Esta acción será definitiva",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Borrar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true },

function(isConfirm){
if (isConfirm) {
swal({
title: "Registro Eliminado Correctamente",

type: "success",

confirmButtonColor: "#5f022a",

confirmButtonText: "Ok",

closeOnConfirm: false,
closeOnCancel: false },okCallback)
}
});
}