<?php

namespace app\controllers;

use Yii;
use app\models\Entrenadores;
use app\models\EntrenadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\db\Query;



/**
 * EntrenadoresController implements the CRUD actions for Entrenadores model.
 */
class EntrenadoresController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function actionEntrendadoresindex() {
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Entrenadores::find()
        ->select('cod_entrenador,nombre,apellidos,cargo,telefono')


        ]);
        return $this->render("entrenadoresv", [
                    "resultados" => $dataProvider,
        ]);
    }
    public function actionEntrenadoresuser() {
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Entrenadores::find()
        ->select('cod_entrenador,nombre,apellidos,cargo,telefono')


        ]);
        return $this->render("entrenadoresuser", [
                    "resultados" => $dataProvider,
        ]);
    }
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entrenadores models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EntrenadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entrenadores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entrenadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Entrenadores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             return $this->redirect(['entrendadoresindex']);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entrenadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('../entrendadoresindex');
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entrenadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['entrendadoresindex']);
    }

    /**
     * Finds the Entrenadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entrenadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Entrenadores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
