<?php

namespace app\controllers;

use Yii;
use app\models\Juegan;
use app\models\JueganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * JueganController implements the CRUD actions for Juegan model.
 */
class JueganController extends Controller
{
    /**
     * {@inheritdoc}
     */
     public $tc;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Juegan models.
     * @return mixed
     */
    public function actionIndex()
    {
         $dataProvider = new ActiveDataProvider([
            'query' => Juegan::find(),
             'pagination'=>false,
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    
    }

    /**
     * Displays a single Juegan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Juegan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate($id,$cod)
//    {
////        $model = new Juegan();
////        
////       
////        
////
////        if ($model->load(Yii::$app->request->post()) && $model->save()) {
////   
////        
////           $id=$model->cod_jugador;
////           $model->save();
////
////           return $this->redirect(array('juegan/puntos2', 'cod' => $model->cod_partido));
////        }
////
////        return $this->render('create', [
////            'model' => $model,
////            'id'=>$id,
////            'cod'=>$cod,
////         
////        ]);
//    }
//    NOS MOSTRARA LOS JUGADORES CON SUS IMAGENES EN LA VISTA ACTUALIZARJUEGAN pero 
public function actionActualizar($cod){
  $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,numero,puesto,imagen,j.cod_jugador,cod_juegan')
                    ->leftJoin('juegan j', 'jugadores.cod_jugador=j.cod_jugador')
                    ->where('estado=1')
          ->andWhere('agente_libre=0'),
          
            'pagination'=>false,
        ]);
        return $this->render("actualizarjuegan", [
                    'dataProvider' => $dataProvider,
                    'cod'=>$cod,
        ]);
    }
   
  
//     public function actionPuntos2($cod)
//    {
//$dataProvider = new \yii\data\SqlDataProvider([
//            'sql' => 'SELECT * '
//    . 'FROM jugadores j '
//    . 'WHERE j.cod_jugador NOT IN'
//    . '(SELECT j.cod_jugador FROM jugadores INNER JOIN juegan j ON jugadores.cod_jugador = j.cod_jugador WHERE j.cod_partido='.$cod.')'
//    . 'AND j.estado=1',
//          
//        ]);   
//
//        return $this->render('puntosjugador2', [
//            'dataProvider' => $dataProvider,
//            'cod'=>$cod,
//        ]);
//    }

 
    public function actionUpdate($codjugador,$cod)
    {
        $codjuegan= Juegan::find()
                ->select('cod_juegan')
                ->where('juegan.cod_jugador='.$codjugador)
                ->andWhere('cod_partido='.$cod)
                ->one();
        $model= $this->findModel($codjuegan);
       
       

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['juegan/actualizar', 'cod' => $cod]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing Juegan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Juegan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Juegan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Juegan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}