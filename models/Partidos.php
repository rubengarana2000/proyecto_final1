<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $cod_partido
 * @property int $puntos_rivales
 * @property string $nombre_rival
 * @property string|null $imagen
 * @property string $estadio
 * @property string $mes
 * @property string|null $fecha
 *
 * @property Juegan[] $juegans
 * @property Jugadores[] $codJugadors
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */public $puntos;
    public $TC;
    public $T3;
    public $TL;
    public $robos;
    public $nosotros;
    public $resultado;
    public  $codigo;
    public $media;
    public  $cod_jugador;
    public $minutos;
    public $video;
     public $fechames;

    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['puntos_rivales', 'nombre_rival', 'estadio', 'fecha'], 'required'],
            [['puntos_rivales'], 'integer'],
            [['fecha'], 'safe'],
            [['nombre_rival'], 'string', 'max' => 25],
            [['imagen'], 'string', 'max' => 500],
            [['estadio', 'mes'], 'string', 'max' => 255],
            [['video'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg,mp3,mp4'],
            [['puntos_rivales'],'integer','max' => 200, 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_partido' => 'Cod Partido',
            'puntos_rivales' => 'Puntos Rivales',
            'nombre_rival' => 'Nombre Rival',
            'imagen' => 'Imagen',
            'estadio' => 'Estadio',
            'mes' => 'Mes',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_partido' => 'cod_partido']);
    }

    /**
     * Gets query for [[CodJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['cod_jugador' => 'cod_jugador'])->viaTable('juegan', ['cod_partido' => 'cod_partido']);
    }
}
