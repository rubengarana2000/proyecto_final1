<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $cod_entrenador
 * @property string $nombre
 * @property string $apellidos
 * @property string $cargo
 * @property int|null $num_victorias
 * @property int|null $num_derrotas
 * @property string|null $imagen
 * @property string|null $telefono
 *
 * @property Entrenamientos[] $entrenamientos
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'cargo'], 'required'],
            [['num_victorias', 'num_derrotas'], 'integer'],
            [['nombre', 'apellidos'], 'match','pattern' => '/^[a-zá-éíóú.-.\s]+$/i', 'message' => 'Solo se aceptan caracteres de de la A hasta la Z.'],
            [['cargo'], 'string', 'max' => 20],
            [['imagen'], 'string', 'max' => 500],
            [['telefono'], 'integer',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_entrenador' => 'Cod Entrenador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'cargo' => 'Cargo',
            'num_victorias' => 'Num Victorias',
            'num_derrotas' => 'Num Derrotas',
            'imagen' => 'Imagen',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Entrenamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenamientos()
    {
        return $this->hasMany(Entrenamientos::className(), ['cod_entrenador' => 'cod_entrenador']);
    }
}
