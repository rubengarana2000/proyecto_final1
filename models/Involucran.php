<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "involucran".
 *
 * @property int $cod_involucran
 * @property int|null $cod_jugador
 * @property int|null $cod_traspaso
 * @property string|null $equipo_inicial
 * @property string|null $equipo_final
 * @property string|null $imagen
 *
 * @property Jugadores $codJugador
 * @property Traspasos $codTraspaso
 */
class Involucran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'involucran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador', 'cod_traspaso'], 'integer'],
            [['equipo_inicial', 'equipo_final'], 'string', 'max' => 30],
            [['imagen'], 'string', 'max' => 500],
            [['cod_jugador', 'cod_traspaso'], 'unique', 'targetAttribute' => ['cod_jugador', 'cod_traspaso']],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
            [['cod_traspaso'], 'exist', 'skipOnError' => true, 'targetClass' => Traspasos::className(), 'targetAttribute' => ['cod_traspaso' => 'cod_traspaso']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_involucran' => 'Cod Involucran',
            'cod_jugador' => 'Cod Jugador',
            'cod_traspaso' => 'Cod Traspaso',
            'equipo_inicial' => 'Equipo Inicial',
            'equipo_final' => 'Equipo Final',
            'imagen' => 'Imagen',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[CodTraspaso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodTraspaso()
    {
        return $this->hasOne(Traspasos::className(), ['cod_traspaso' => 'cod_traspaso']);
    }
}
