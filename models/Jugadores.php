<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $cod_jugador
 * @property string $nombre
 * @property string|null $apellidos
 * @property int|null $lesion
 * @property int|null $años_carrera
 * @property string $puesto
 * @property int|null $envergadura
 * @property int|null $altura
 * @property int $numero
 * @property string|null $imagen
 * @property float|null $puntos_partido
 * @property float|null $rebotes_partido
 * @property float|null $asistencias_partido
 * @property int|null $agente_libre
 * @property int|null $estado
 * @property string|null $equipo_anterior
 * @property string|null $liga
 * @property int|null $años
 * @property string|null $fecha_nacimiento
 *
 * @property Contratos[] $contratos
 * @property Involucran[] $involucrans
 * @property Traspasos[] $codTraspasos
 * @property Juegan[] $juegans
 * @property Partidos[] $codPartidos
 * @property Nacionalidades[] $nacionalidades
 * @property Realizan[] $realizans
 */
class Jugadores extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public $puntos;
    public $tapones;
    public $robos;
    public $asistencias;
    public $TC;
    public $T3;
    public $TL;
    public $rebotes;
    public $minutos;
    public $cod_partido;
    public $imagen;
    public $cod_juegan;
    public $eventImage;
    public $fecha;
   public $partidosjugados;

    public static function tableName() {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre', 'puesto'], 'required'],
            [['lesion', 'numero', 'agente_libre', 'estado', 'años'], 'integer'],
            [['puntos_partido', 'rebotes_partido', 'asistencias_partido'], 'number'],
            [['fecha_nacimiento'], 'safe'],
            [['nombre', 'apellidos'], 'match','pattern' => '/^[a-zá-éíóú]+$/i', 'message' => 'Solo se aceptan caracteres de de la A hasta la Z.'],
            [['puesto'], 'string', 'max' => 10],
            [['imagen'], 'string', 'max' => 500],
            [['equipo_anterior', 'liga'], 'string', 'max' => 255],
            [['eventImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['años_carrera'], 'integer', 'max' => 30, 'min' => 0], 
            [['envergadura', 'altura'], 'integer', 'max' => 300, 'min' => 0],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'cod_jugador' => 'Cod Jugador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'lesion' => 'Lesión',
            'años_carrera' => 'Años Carrera',
            'puesto' => 'Puesto',
            'envergadura' => 'Envergadura',
            'altura' => 'Altura',
            'numero' => 'Número',
            'imagen' => 'Imagen',
            'puntos_partido' => 'Puntos Partido',
            'rebotes_partido' => 'Rebotes Partido',
            'asistencias_partido' => 'Asistencias Partido',
            'agente_libre' => 'Agente Libre',
            'estado' => 'Estado',
            'equipo_anterior' => 'Equipo Anterior',
            'liga' => 'Liga',
            'años' => 'Años',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos() {
        return $this->hasMany(Contratos::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Involucrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvolucrans() {
        return $this->hasMany(Involucran::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[CodTraspasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodTraspasos() {
        return $this->hasMany(Traspasos::className(), ['cod_traspaso' => 'cod_traspaso'])->viaTable('involucran', ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans() {
        return $this->hasMany(Juegan::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[CodPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPartidos() {
        return $this->hasMany(Partidos::className(), ['cod_partido' => 'cod_partido'])->viaTable('juegan', ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Nacionalidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidades() {
        return $this->hasMany(Nacionalidades::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans() {
        return $this->hasMany(Realizan::className(), ['cod_jugador' => 'cod_jugador']);
    }

}
