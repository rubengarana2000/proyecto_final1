<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realizan".
 *
 * @property int $cod_realizan
 * @property int $cod_jugador
 * @property int $cod_entrenamiento
 * @property int $distancia_jugador
 * @property int $calorias_jugador
 * @property string $fecha
 *
 * @property Entrenamientos $codEntrenamiento
 * @property Jugadores $codJugador
 */
class Realizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $nombre;
    public $apellidos;
    public  $nom;
    public static function tableName()
    {
        return 'realizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador', 'cod_entrenamiento','fecha'], 'required'],
            [['cod_jugador', 'cod_entrenamiento', 'distancia_jugador', 'calorias_jugador'], 'integer'],
            [['fecha'], 'safe'],
            [['cod_entrenamiento', 'cod_jugador', 'fecha'], 'unique', 'targetAttribute' => ['cod_entrenamiento', 'cod_jugador', 'fecha']],
            [['cod_jugador', 'cod_entrenamiento', 'fecha'], 'unique', 'targetAttribute' => ['cod_jugador', 'cod_entrenamiento', 'fecha']],
            [['cod_entrenamiento'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenamientos::className(), 'targetAttribute' => ['cod_entrenamiento' => 'cod_entrenamiento']],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
            [['distancia_jugador'],'integer','max' => 10000, 'min' => 0],
            [['calorias_jugador'],'integer','max' => 20000, 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_realizan' => 'Cod Realizan',
            'cod_jugador' => 'Cod Jugador',
            'cod_entrenamiento' => 'Cod Entrenamiento',
            'distancia_jugador' => 'Distancia',
            'calorias_jugador' => 'Calorías',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[CodEntrenamiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntrenamiento()
    {
        return $this->hasOne(Entrenamientos::className(), ['cod_entrenamiento' => 'cod_entrenamiento']);
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }
//     public function afterFind() {
//        parent::afterFind();
//        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
//    }
//    
//    public function beforeSave($insert) {
//        parent::beforeSave($insert);
//        $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
//        return true;
//    }
}
