<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contratos".
 *
 * @property int $cod_contrato
 * @property int $año1
 * @property int|null $año2
 * @property int|null $año3
 * @property int|null $opcion_jugador
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 * @property int|null $clausula_antitraspaso
 * @property int|null $cod_jugador
 *
 * @property Jugadores $codJugador
 */
class Contratos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $salario;
     public $margens;
     public $margeni;
     public $nombre;
     public $apellidos;

    public static function tableName()
    {
        return 'contratos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['año1'], 'required'],
            [['opcion_jugador', 'clausula_antitraspaso', 'cod_jugador'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
//            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
            [['año1', 'año2', 'año3'],'integer','max' => 50000000, 'min' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_contrato' => 'Cod Contrato',
            'año1' => 'Año1',
            'año2' => 'Año2',
            'año3' => 'Año3',
            'opcion_jugador' => 'Opción Jugador',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'clausula_antitraspaso' => 'Cláusula Antitraspaso',
            'cod_jugador' => 'Cod Jugador',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }
}
