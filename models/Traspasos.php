<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "traspasos".
 *
 * @property int $cod_traspaso
 * @property string|null $fecha
 *
 * @property Involucran[] $involucrans
 * @property Jugadores[] $codJugadors
 */
class Traspasos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'traspasos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_traspaso' => 'Cod Traspaso',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Involucrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvolucrans()
    {
        return $this->hasMany(Involucran::className(), ['cod_traspaso' => 'cod_traspaso']);
    }

    /**
     * Gets query for [[CodJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['cod_jugador' => 'cod_jugador'])->viaTable('involucran', ['cod_traspaso' => 'cod_traspaso']);
    }
}
