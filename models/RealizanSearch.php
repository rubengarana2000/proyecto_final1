<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Realizan;

/**
 * RealizanSearch represents the model behind the search form of `app\models\Realizan`.
 */
class RealizanSearch extends Realizan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_realizan', 'cod_jugador', 'cod_entrenamiento', 'distancia_jugador', 'calorias_jugador'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Realizan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cod_realizan' => $this->cod_realizan,
            'cod_jugador' => $this->cod_jugador,
            'cod_entrenamiento' => $this->cod_entrenamiento,
            'distancia_jugador' => $this->distancia_jugador,
            'calorias_jugador' => $this->calorias_jugador,
            'fecha' => $this->fecha,
        ]);

        return $dataProvider;
    }
}
