<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenamientos".
 *
 * @property int $cod_entrenamiento
 * @property int|null $duracion
 * @property int|null $cod_entrenador
 * @property string|null $Tipo
 *
 * @property Entrenadores $codEntrenador
 * @property Realizan[] $realizans
 */
class Entrenamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion', 'cod_entrenador'], 'integer'],
            [['Tipo'], 'string', 'max' => 20],
            [['cod_entrenador'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['cod_entrenador' => 'cod_entrenador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_entrenamiento' => 'Cod Entrenamiento',
            'duracion' => 'Duracion',
            'cod_entrenador' => 'Cod Entrenador',
            'Tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[CodEntrenador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntrenador()
    {
        return $this->hasOne(Entrenadores::className(), ['cod_entrenador' => 'cod_entrenador']);
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans()
    {
        return $this->hasMany(Realizan::className(), ['cod_entrenamiento' => 'cod_entrenamiento']);
    }
}
